<?php
error_reporting(0);
include('../Modelo/prestamoModel.php');
include('../Modelo/conexionModel.php');
$data = array("exito"=>1,"mensaje"=>"","datos" => array());
extract($_POST);
$Prestamo = new Prestamo();
$Prestamo->setTxtTipoDocumento($txtTipoDocumento);
$Prestamo->setNumDocumento($numDocumento);
$Prestamo->setNumFicha($numFicha);
$Prestamo->setTxtDescripcion($txtDescripcion);
$Conexion = new Conexion();
if($accion == 1){
    $sql = "SELECT * FROM prestamo WHERE documento_prestamo = '". $Prestamo->getNumDocumento() ."' AND tipodocumento_prestamo = '". $Prestamo->getTxtTipoDocumento() ."'";
    $Conexion->ejecutar($sql);
    if($Conexion->obtenerRegistro() == 0){
        $sql = "INSERT INTO prestamo(tipodocumento_prestamo,documento_prestamo,descripcion_prestamo,fechaentrega_prestamo,ficha_prestamo,id_sede)
                VALUES('". $Prestamo->getTxtTipoDocumento() ."','". $Prestamo->getNumDocumento() ."','". $Prestamo->getTxtDescripcion() ."',CURRENT_TIMESTAMP,'". $Prestamo->getNumFicha() ."','1')";
        $Conexion->ejecutar($sql);
        $data['mensaje'] = "Se ha prestado correctamente.";
    }else{
        $data['exito']=0;
        $data['mensaje']="Ya se le ha prestado a esta persona";
    }
}else if($accion == 2){
    $sql = "SELECT * FROM prestamo WHERE documento_prestamo = '". $Prestamo->getNumDocumento() ."' AND tipodocumento_prestamo = '". $Prestamo->getTxtTipoDocumento() ."' AND recibido_prestamo = '0'";
    $Conexion->ejecutar($sql);
    if($Conexion->obtenerRegistro() == 1){
        $fila = $Conexion->obtenerObjeto();
        $data['datos']['descripcion'] = $fila->descripcion_prestamo;
    }else{
        $data['exito']=0;
        $data['mensaje']="No coinciden el numero del documento";
    }
}else{
    $sql = "SELECT * FROM prestamo WHERE documento_prestamo = '". $Prestamo->getNumDocumento() ."' AND tipodocumento_prestamo = '". $Prestamo->getTxtTipoDocumento() ."'";
    $Conexion->ejecutar($sql);
    if($Conexion->obtenerRegistro() == 1){
        if($recibido == true){
            $sql = "UPDATE prestamo SET recibido_prestamo = '1', novedad_prestamo = '". $txtNovedad ."', fecharecibido_prestamo = CURRENT_TIMESTAMP WHERE documento_prestamo = '". $Prestamo->getNumDocumento() ."' AND tipodocumento_prestamo = '". $Prestamo->getTxtTipoDocumento() ."'";
        }else{
            $sql = "UPDATE prestamo SET recibido_prestamo = '0', novedad_prestamo = '". $txtNovedad ."', fecharecibido_prestamo = CURRENT_TIMESTAMP WHERE documento_prestamo = '". $Prestamo->getNumDocumento() ."' AND tipodocumento_prestamo = '". $Prestamo->getTxtTipoDocumento() ."'";
        }
        $Conexion->ejecutar($sql);
        $data['mensaje']="Prestamo recibido";
    }else{
        $data['exito']=0;
        $data['mensaje']="Lo sentimos, se ha producido un error";
    }
}

$Conexion->cerrarConexion();
echo json_encode($data);
?>