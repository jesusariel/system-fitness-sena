<?php

include '../../Modelo/ubicacion/regionalModel.php';
include '../../Modelo/conexionModel.php';

$retorno=array('mensaje' => '','exito' => 1,'datos' => "");
$txtRegional=filter_input(INPUT_POST, 'txtRegional');
$slcDepartamento=filter_input(INPUT_POST, 'slcDepartamento');
$slcSubdirector=filter_input(INPUT_POST, 'slcSubdirector');
$slcEstado=filter_input(INPUT_POST, 'slcEstado');
$accion=filter_input(INPUT_POST, 'accion');
$id=filter_input(INPUT_POST, 'id');

$Regional= new Regional();
$Regional->setTxtRegional($txtRegional);
$Regional->setSlcDepartamento($slcDepartamento);
$Regional->setSlcSubdirector($slcSubdirector);
$Regional->setSlcEstado($slcEstado);

$Conexion = new Conexion();
if($accion == 1){
    $sql = "INSERT INTO regional (regional_regional, departamento_regional, subdirector_regional, estado_regional)
		values ('" . $Regional->getTxtRegional() . "', '" . $Regional->getSlcDepartamento() . "', '" . $Regional->getSlcSubdirector() . "', '" . $Regional->getSlcEstado() . "')";
	
        $Conexion->ejecutar($sql);        
        $retorno['mensaje'] = "Corecto";
}else if($accion == 2){
    $sql = "SELECT * FROM regional WHERE id_regional = '$id'";
    $Conexion->ejecutar($sql);
    if($Conexion->obtenerRegistro() == 1){
        $fila = $Conexion->obtenerObjeto();
        $retorno['datos'] = $fila;
    }else{
        $retorno['exito']=0;
        $retorno['mensaje']="Se produjo un error";
    }
}else if($accion == 3){
    $sql = "UPDATE regional SET regional_regional = '". $Regional->getTxtRegional() ."', estado_regional = '". $Regional->getSlcEstado() ."', departamento_regional = '". $Regional->getSlcDepartamento() ."', subdirector_regional = '". $Regional->getSlcSubdirector() ."' WHERE id_regional = '". $id ."'";
    $Conexion->ejecutar($sql);
    $retorno['mensaje']="Se actualizo la informacion";
}else{
    $sql = "DELETE FROM regional WHERE id_regional = '". $id ."'";
    $Conexion->ejecutar($sql);
    $retorno['mensaje']="Se ha eliminado esa region";
}

echo json_encode($retorno);
?>