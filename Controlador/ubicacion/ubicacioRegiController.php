<?php
error_reporting(0);
include('../../Modelo/ubicacion/regionModel.php');
include('../../Modelo/conexionModel.php');
$data = array("exito"=>1,"mensaje"=>"","datos" => array());
extract($_POST);
$filImagen=$_FILES['filImagen'];
$Region = new Region();
$Region->setTxtNombreUbicacion($txtNombreUbicacion);
$Region->setSlcPais($slcPais);
$Region->setFilImagen($filImagen);
$Region->setSlcEstado($slcEstado);
$Conexion = new Conexion();
if($accion == 1){
    $sql = "SELECT * FROM region WHERE descripcion_region = '". $Region->getTxtNombreUbicacion() ."' AND id_pais = '". $Region->getSlcPais() ."'";
    $Conexion->ejecutar($sql);
    if($Conexion->obtenerRegistro() == 0){
        $ruta = "../../Vista/img/Ubicacion/";
        if(file_exists($ruta . $Region->getFilImagen()['name'])){
            $extension = explode("/", $Region->getFilImagen()["type"]);
            $filImagen['name'] = basename($Region->getFilImagen()['name'], "." . $extension[1]) . rand(1,100) . "." . $extension[1];
            $Region->setFilImagen($filImagen);
        }
        move_uploaded_file($Region->getFilImagen()['tmp_name'], $ruta . $Region->getFilImagen()['name']);
        $sql = "INSERT INTO region(descripcion_region,estado_region,imagen_region,id_pais)
                VALUES('". $Region->getTxtNombreUbicacion() ."','". $Region->getSlcEstado() ."','". $Region->getFilImagen()['name'] ."','". $Region->getSlcPais() ."')";
        $Conexion->ejecutar($sql);
        $data['mensaje'] = "Se ha registrado correctamente.";
    }else{
        $data['exito']=0;
        $data['mensaje']="Ya se ha registrado esta region";
    }
}else if($accion == 2){
    $sql = "SELECT * FROM region WHERE id_region = '". $id ."'";
    $Conexion->ejecutar($sql);
    if($Conexion->obtenerRegistro() == 1){
        $fila = $Conexion->obtenerObjeto();
        $data['datos'] = $fila;
    }else{
        $data['exito']=0;
        $data['mensaje']="Se produjo un error";
    }
}else if($accion == 3){
    if($Region->getFilImagen()['name'] !== ''){
        $sql = "UPDATE region SET descripcion_region = '". $Region->getTxtNombreUbicacion() ."', estado_region = '". $Region->getSlcEstado() ."', imagen_region = '". $Region->getFilImagen()['name'] ."', id_pais = '". $Region->getSlcPais() ."' WHERE id_region = '". $id ."'";
    }else{
        $sql = "UPDATE region SET descripcion_region = '". $Region->getTxtNombreUbicacion() ."', estado_region = '". $Region->getSlcEstado() ."', id_pais = '". $Region->getSlcPais() ."' WHERE id_region = '". $id ."'";
    }
    $Conexion->ejecutar($sql);
    $data['mensaje']="Se actualizo la informacion";
}else{
     $sql = "SELECT imagen_region FROM region WHERE id_region = '". $id ."'";
    $Conexion->ejecutar($sql);
    unlink("../../Vista/img/Ubicacion/". $Conexion->obtenerObjeto()->imagen_region);
    $sql = "DELETE FROM region WHERE id_region = '". $id ."'";
    $Conexion->ejecutar($sql);
    $data['mensaje']="Se ha eliminado esa region";
}

$Conexion->cerrarConexion();
echo json_encode($data);
?>