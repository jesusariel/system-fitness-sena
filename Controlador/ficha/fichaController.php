<?php

include '../../Modelo/ficha/fichaModel.php';
//error_reporting(0);
$retorno=array();
$numFicha=filter_input(INPUT_POST, 'numFicha');
$txtJornada=filter_input(INPUT_POST, 'txtJornada');
$datFechaI=filter_input(INPUT_POST, 'datFechaI');
$datFechaF=filter_input(INPUT_POST, 'datFechaF');
$id=filter_input(INPUT_POST, 'id');
$accion=filter_input(INPUT_POST, 'accion');


$Ficha= new Ficha();
$Ficha->setNumeroFicha($numFicha);
$Ficha->setJornada($txtJornada);
$Ficha->setFechaInicio($datFechaI);
$Ficha->setFechaFin($datFechaF);

if($accion == 1){
    $Ficha->insertar();
}else if($accion == 2){
    $Ficha->consulta($id);
}else if($accion == 3){
    $Ficha->actualizar($id);
}else{
    $Ficha->eliminar($id);
}

$retorno['mensaje']=$Ficha->resultado;
$retorno['exito']=$Ficha->exito;
$retorno['datos']=$Ficha->datos;
echo json_encode($retorno);
?>