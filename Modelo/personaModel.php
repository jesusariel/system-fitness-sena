<?php
require_once "ConexionModel.php";
class Persona{ 

		private $persona;
		private $nombre;
		private $apellido;
		private $edad;
		private $correo;
		private $eps;
		private $rh;
		
		public function getPersona(){
			return $this->persona;
		}
		public function setPersona($persona){
			 $this->persona = $persona;
	}

		public function getNombre(){
			return $this->nombre;
		}
		public function setNombre($nombre){
			 $this->nombre = $nombre;
	}

		public function getApellido(){
			return $this->apellido;
		}
		public function setApellido($apellido){
			 $this->apellido = $apellido;
	}

		public function getEdad(){
			return $this->edad;
		}
		public function setEdad($edad){
			 $this->edad = $edad;
	}

		public function getCorreo(){
			return $this->correo;
		}
		public function setCorreo($correo){
			 $this->correo = $correo;
	}

		public function getEps(){
			return $this->eps;
		}
		public function setEps($eps){
			 $this->eps = $eps;
	}

		public function getRh(){
			return $this->rh;
		}
		public function setRh($rh){
			 $this->rh = $rh;
	}

        public function insertPersona(){
            $Conexion = new Conexion();

            $sentenciaSql= "INSERT INTO persona(nombre_persona,apellido_persona,edad_persona,correo_persona,eps_persona,rh_persona) VALUES ('$this->nombre','$this->apellido','$this->edad','$this->correo','$this->eps','$this->rh')";
            $Conexion->ejecutar($sentenciaSql);
            $Conexion->cerrarConexion();
  
        }
        public function updatePersona(){
            $Conexion = new Conexion();

            $sentenciaSql== "UPDATE persona SET nombre_persona='$this->nombre',apellido_persona='$this->apellido',edad_persona='$this->edad',correo_persona='$this->correo',eps_persona='$this->eps',rh_persona='$this->rh' WHERE id_persona='$this->persona'";
            $Conexion->ejecutar($sentenciaSql);
            $Conexion->cerrarConexion();
  
        }
        public function deletePersona(){
            $Conexion = new Conexion();

            $sentenciaSql= "DELETE FROM persona WHERE id_persona='$this->persona'";
            $Conexion->cerrarConexion();
  
        }





 
}
?>