<?php
class Conexion {
    private $conexion;
    private $recordSet;
   
    function __construct(){
        $this->conexion = new mysqli('localhost','root','','fitness_sena');
    } 
    
    public function ejecutar($sentenciaSql) {
        $this->recordSet = $this->conexion->query($sentenciaSql);
        if($this->recordSet == FALSE){
            throw new Exception("Error al conectar");
        }
    }
   
    public function obtenerObjeto() {
        return $this->recordSet->fetch_object();
    }
   
    public function obtenerRegistro() {
        return mysqli_affected_rows($this->conexion);
    }
   
    public function cerrarConexion(){
        $this->conexion->close();
    }
   
    
}
?>