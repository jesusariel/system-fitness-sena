<?php

class Profesional {
	private $txtRegional;
	private $txtNumeroDoc;
	private $slcTipo;
	private $txtTelefono;
    private $txtCorreo;

	//Nombre
	public function setTxtNombre($txtNombre){
		$this->txtNombre=$txtNombre;
	}
	public function getTxtNombre(){
		return $this->txtNombre;
	}

	//Numero Documento
	public function setNumbrNumeroDoc($numbrNumeroDoc){
		$this->numbrNumeroDoc=$numbrNumeroDoc;
	}
	public function getNumbrNumeroDoc(){
		return $this->numbrNumeroDoc;
	}

	//Tipo Profesional
	public function setSlcTipo($slcTipo){
		$this->slcTipo=$slcTipo;
	}
	public function getSlcTipo(){
		return $this->slcTipo;
	}

	//Telefono
	public function setNumbrTelefono($numbrTelefono){
		$this->numbrTelefono=$numbrTelefono;
	}
	public function getNumbrTelefono(){
		return $this->numbrTelefono;
	}
    //Correo
	public function setTxtCorreo($txtCorreo){
		$this->txtCorreo=$txtCorreo;
	}
	public function getTxtCorreo(){
		return $this->txtCorreo;
	}
}

?>