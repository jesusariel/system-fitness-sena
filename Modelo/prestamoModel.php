<?php
class Prestamo{
    private $txtTipoDocumento;
    private $numDocumento;
    private $numFicha;
    private $txtDescripcion;
    
    public function setTxtTipoDocumento($txtTipoDocumento){
        $this->txtTipoDocumento=$txtTipoDocumento;
    }
    
    public function getTxtTipoDocumento(){
        return $this->txtTipoDocumento;
    }
    
    public function setNumDocumento($numDocumento){
        $this->numDocumento=$numDocumento;
    }
    
    public function getNumDocumento(){
        return $this->numDocumento;
    }
    
    public function setNumFicha($numFicha){
        $this->numFicha=$numFicha;
    }
    
    public function getNumFicha(){
        return $this->numFicha;
    }
    
    public function setTxtDescripcion($txtDescripcion){
        $this->txtDescripcion=$txtDescripcion;
    }
    
    public function getTxtDescripcion(){
        return $this->txtDescripcion;
    }
}
?>