<?php  
class Sede{
	private $nombre;
	private $municipio;
	private $estado;

	public function getNombre(){
		return $this->$nombre;
	}

	public function setNombre($nombre){
		$this->$nombre = $nombre;
	}

	public function getMunicipio(){
		return $this->$municipio;	
	}

	public function setMunicipio($municipio){
		$this->$municipio = $municipio;	
	}

	public function getEstado(){
		return $this->$estado;
	}

	public function setEstado($estado){
		$this->$estado = $estado;
	}	
}
?>
