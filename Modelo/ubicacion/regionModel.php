<?php

class Region{
	private $txtNombreUbicacion;
	private $slcPais;
	private $filImagen;
	private $slcEstado;

	public function setTxtNombreUbicacion($txtNombreUbicacion){
	    $this->txtNombreUbicacion = $txtNombreUbicacion;
	  }
	public function getTxtNombreUbicacion(){
	    return $this->txtNombreUbicacion;
	  }
	  

	  //Pais
	  public function getSlcPais(){
	    return $this->slcPais;
	  }
	  public function setSlcPais($slcPais){
	    $this->slcPais = $slcPais;
	  }
	  //Imagen
	  public function getFilImagen(){
	    return $this->filImagen;
	  }
	  public function setFilImagen($filImagen){
	    $this->filImagen = $filImagen;
	  }
	  
	  //Estado
	  public function getSlcEstado(){
	    return $this->slcEstado;
	  }
	  public function setSlcEstado($slcEstado){
	    $this->slcEstado = $slcEstado;
	  }
}
  
?>