<?php

class Regional {
	private $txtRegional;
	private $slcDepartamento;
	private $slcSubdirector;
	private $slcEstado;

	//Regional
	public function setTxtRegional($txtRegional){
		$this->txtRegional=$txtRegional;
	}
	public function getTxtRegional(){
		return $this->txtRegional;
	}

	//Departamento
	public function setSlcDepartamento($slcDepartamento){
		$this->slcDepartamento=$slcDepartamento;
	}
	public function getSlcDepartamento(){
		return $this->slcDepartamento;
	}

	//Subdirector
	public function setSlcSubdirector($slcSubdirector){
		$this->slcSubdirector=$slcSubdirector;
	}
	public function getSlcSubdirector(){
		return $this->slcSubdirector;
	}

	//Estado
	public function setSlcEstado($slcEstado){
		$this->slcEstado=$slcEstado;
	}
	public function getSlcEstado(){
		return $this->slcEstado;
	}
}

?>