<?php
require_once "ConexionModel.php";

	class Usuario{ 

		private $usuario;
		private $persona;
        private $tipousuario;
		private $contrasena;
		private $google;
        private $correo;
 
		public function getUsuario(){
			return $this->usuario;
		}
		public function setUsuario($usuario){
			 $this->usuario = $usuario;
	}	
		public function getPersona(){
			return $this->persona;
		}
		public function setPersona($persona){
			 $this->persona = $persona;
	}
		public function getTipoUsuario(){
			return $this->tipousuario;
		}
		public function setTipoUsuario($tipousuario){
			 $this->tipousuario = $tipousuario;
	}

		public function getContrasena(){
			return $this->contrasena;
		}
		public function setContrasena($contrasena){
			 $this->contrasena = $contrasena;
	}

		public function getGoogle(){
			return $this->google;
		}
		public function setGoogle($google){
			 $this->google = $google;
	}

		public function getCorreo(){
			return $this->correo;
		}
		public function setCorreo($correo){
			 $this->correo = $correo;
	}    

        public function insertUsuario(){
	        $Conexion = new Conexion();
	        
	        $sentenciaSql = "SELECT id_persona FROM persona WHERE correo_persona='$this->correo'";
	        $Conexion->ejecutar($sentenciaSql);
	        $persona=$Conexion->obtenerObjeto();

	        $sentenciaSql= "INSERT INTO usuario(id_persona,id_tipousuario,contrasena_usuario,google_usuario) VALUES ('$persona->id_persona','$this->tipousuario','$this->contrasena','$this->google')";      
	        $Conexion->ejecutar($sentenciaSql);
	        $Conexion->cerrarConexion();
        }

        public function updatePersona(){
            $Conexion = new Conexion();

            $sentenciaSql== "UPDATE usuario SET contrasena_usuario='$this->contrasena' WHERE id_usuario='$this->usuario'";
            $Conexion->ejecutar($sentenciaSql);
            $Conexion->cerrarConexion();
  
        }
        public function deletePersona(){
            $Conexion = new Conexion();

            $sentenciaSql= "DELETE FROM Persona WHERE id_usuario='$this->usuario'";
            $Conexion->cerrarConexion();
  
        }

}

?>