<?php
require('configuracionModel.php');

class Conexion {
    private $conex;
    private $recordSet;
   
    function __construct(){
        $this->conex = new mysqli(SERVERNAME,USER,PASSWORD,DATABASE);
    } 

    
    public function ejecutar($sentenciaSql) {
        $this->recordSet = $this->conex->query($sentenciaSql);
        if($this->recordSet == FALSE){
            throw new Exception("Error al ejecutar la sentencia");
        }
    }
   
    public function obtenerObjeto() {
        return $this->recordSet->fetch_object();
    }
    
    public function obtenerArray() {
        return mysqli_fetch_array($this->recordSet);
    }
   
    public function obtenerRegistro() {
        return mysqli_affected_rows($this->conex);
    }
   
    public function cerrarConexion(){
        $this->conex->close();
    }
   
    
}
?>