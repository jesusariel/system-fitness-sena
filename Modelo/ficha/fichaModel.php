<?php

include '../../Modelo/ConexionModel.php';

class Ficha {
	private $numeroFicha;
	private $jornada;
	private $fechaInicio;
	private $fechaFin;
	public $exito;
	public $resultado;
	public $datos;

	//numero Ficha
	public function setNumeroFicha($numeroFicha){
		$this->numeroFicha=$numeroFicha;
	}
	public function getNumeroFicha(){
		return $this->numeroFicha;
	}

	//jornada
	public function setJornada($jornada){
		$this->jornada=$jornada;
	}
	public function getJornada(){
		return $this->jornada;
	}

	//fecha inicio
	public function setFechaInicio($fechaInicio){
		$this->fechaInicio=$fechaInicio;
	}
	public function getFechaInicio(){
		return $this->fechaInicio;
	}

	//fecha fin
	public function setFechaFin($fechaFin){
		$this->fechaFin=$fechaFin;
	}
	public function getFechaFin(){
		return $this->fechaFin;
	}

	public function insertar(){
		$Conexion = new Conexion();
		$sql = "SELECT * FROM ficha WHERE ficha_ficha = '". $this->numeroFicha ."'";
		$Conexion->ejecutar($sql);
		if($Conexion->obtenerRegistro() >= 1){
		    $this->exito = 0;
		    $this->resultado = "La ficha, se encuentra registrada";
		}else if((strtotime($this->fechaInicio) > strtotime($this->fechaFin))){
		    $this->exito = 0;
		    $this->resultado = "Verifique las fechas, porfavor.";
		}else{
		    $sql = "INSERT INTO ficha(ficha_ficha, fecha_inicio_ficha, fecha_fin_ficha, jornada_ficha, id_ambiente, id_programa_formacion)
				values ('" . $this->numeroFicha . "', '" . $this->fechaInicio . "', '" . $this->fechaFin . "', '" . $this->jornada . "', '1', '1')";
			
		        $Conexion->ejecutar($sql);
		        
				$this->exito = 1;
		        $this->resultado = "Correcto";
		}
	}
	
	public function consulta($id){
		$Conexion = new Conexion();
		$sql = "SELECT * FROM ficha WHERE ficha_ficha = '". $id ."'";
		$Conexion->ejecutar($sql);
	    $fila = $Conexion->obtenerObjeto();
		$this->datos = $fila;
	}
	
	public function actualizar($id){
		$Conexion = new Conexion();
		$sql = "UPDATE ficha SET ficha_ficha = '". $this->numeroFicha ."', fecha_inicio_ficha = '". $this->fechaInicio ."', fecha_fin_ficha = '". $this->fechaFin ."', jornada_ficha = '". $this->jornada ."' WHERE ficha_ficha = '". $id ."'";
		$Conexion->ejecutar($sql);
		$this->exito = 1;
		$this->resultado="Se actualizo la informacion";
	}
	
	public function eliminar($id){
		$Conexion = new Conexion();
		$sql = "DELETE FROM ficha WHERE ficha_ficha = '". $id ."'";
		$Conexion->ejecutar($sql);
		$this->exito = 1;
		$this->resultado="Se ha eliminado esa region";
	}

}

?>