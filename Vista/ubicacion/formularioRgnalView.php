<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Creación de Regional | System Fitness SENA</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" type="text/css" href="../../css/style.css">
	<link rel="stylesheet" type="text/css" href="../../css/estiloGlobView.css">
	<link rel="stylesheet" type="text/css" href="../../css/ubicacion/estiloRgnalView.css">
	<script src="../../js/jQuery/jquery-3.3.1.js"></script>
	<script src="../../js/jQuery/jquery.min.js"></script>
	<script src="../../js/funcionMenView.js"></script>
	<script src="../../js/funcionLogiView.js"></script>
	<script src="../../js/regionalView.js"></script>
</head>
<body>
	<header>
		<div class="logo">System Fitness SENA</div>
			<div id="menu">
				<nav >
					<ul>
					
						<li class="usuarioResponsive"><a href="#">Nombre de Usuario</a></li>
						<li><a href="../../../inicioUsuario.html" >Inicio</a></li>
						<li><a href="noticiaView.html" >Noticias</a></li>
						<li><a href="#">Inventario</a></li>
						<li><a href="#">Gimnasio</a></li>
						<li><a href="#" class="activo" >Ubicacion</a></li>
						<li><a href="#">Documentacion</a></li>
						<li class="usuarioResponsive"><a href="#">Cerrar Sesion</a></li>
							<div id="conteUser">
								<div id="btnUser">Nombre de Usuario</div>


							<form class="panelUser">
								<div id="contePanelUser">
								<div id="opcionPanel">Panel de Usuario</div>
								<div id="opcionCerrar"><a href="" id="aCerrar"><span class=''></span> Cerrar Sesion</a></div>
								</div>
							</form>
					
						</div>
					</ul>
				</nav>
			</div>
		<div class="menu-toggle">
			<div class="toggle">
				<span> </span>
				<span> </span>
				<span> </span>
			</div>
		</div>
	</header>
	<div class="menuUbicacion">
		<a href="formularioPaisView.html" class="opcionMenu ">Pais</a>
		<a href="formularioRegiView.html" class="opcionMenu">Region</a>
		<a href="formularioDepaView.html" class="opcionMenu ">Departamento</a>
        <a href="formularioRegionalView.html" class="opcionMenu seleccionado">Regional</a>
		<a href="formularioMuniView.html" class="opcionMenu ">Municipios</a>
		<a href="formularioSedeView.html" class="opcionMenu ">Sede</a>

</div>
<div class="contenedor-General"><!--fin de contenedor general-->

	<div class="conten">
		<form action="javascript: guardar(1,1)" id="form">
            <div class="Titulo">
                    <h2>Crear Regional</h2>
                </div>
            <div class="cinco">
                <div class="inputbox">
                    <input type="text" id="txtRegional" name="txtRegional" required="" pattern="[A-Z a-z]+" maxlength="30">
                    <label >Regional:</label>
                </div>
            </div>
            <div class="cinco">
                <div class="selectbox">
                    <div class="labelSelect">
                        <label for="departamento">Departamento</label>
                    </div>
                        <select id="slcDepartamento" name="slcDepartamento">
							<?php
							include ('../../Modelo/conexionModel.php');
							$Conexion = new Conexion();
							$Conexion->ejecutar('SELECT * FROM departamento;');
							while($fila = $Conexion->obtenerObjeto()){
								echo "<option value='". $fila->descripcion_departamento ."'>". $fila->descripcion_departamento ."</option>";
							}
							?>


                        </select>		
                </div>
            </div>

            <div class="cinco">
                <div class="selectbox">
                    <div class="labelSelect">
                        <label for="subdirector">Subdirector</label>
                    </div>
                        <select id="slcSubdirector" name="slcSubdirector">
                            <option value="Arturo Arango Santos">Arturo Arango Santos</option>
                            <option value="Juan Felipe Rendón Ochoa">Juan Felipe Rendón Ochoa</option>
                            <option value="Luis Alberto Tamayo Manrique">Luis Alberto Tamayo Manrique</option>

                        </select>		
                </div>
            </div>
            <div class="cinco">
                <div class="selectbox">
                    <div class="labelSelect">
                        <label for="estado">Estado</label>
                    </div>
                        <select id="slcEstado" name="slcEstado">
                            <option value="1">Activo</option>
                            <option value="0">Inactivo</option>
                        </select>		
                </div>
            </div>

            <br><br>
            <input type="submit" name="Agregar" id="submit" value="Agregar" class="btn-Enviar">		
            </div>
		</form>
	</div>
	<div class="tituloUbicacion">Regionales Registradas</div>
	<div class="contenedorTable">
		<table>
	          <tr>
	            <td class="titulo">Regional</td>
	            <td class="titulo">Departamento</td>
	            <td class="titulo">Subdirector</td>
                
                <td class="titulo">Acciones</td>
	          </tr>
			  <?php
				$Conexion->ejecutar('SELECT * FROM regional ORDER BY id_regional;');
				while($fila = $Conexion->obtenerObjeto()){
					echo "<tr id='pos". $fila->id_regional ."'>
		            <td>". $fila->regional_regional ."</td>
		            <td>". $fila->departamento_regional ."</td>
		            <td>". $fila->subdirector_regional ."</td>
		            <td><input type='button' value='Editar' class='btnEditar' onclick='litarEditar(". $fila->id_regional .")'><input type='button' value='Eliminar' class='btnEliminar' onclick='eliminar(". $fila->id_regional .")'></td>
		          </tr>";
				}
				$Conexion->cerrarConexion();
			  ?>
		</table>
	</div>
</div><!--fin de contenedor general-->
<!----------modal--------->
	<div class="modal" id="curso1">
		<div class="contenedorModal">
	      <div class="tituloModal">
	       
	        <h2>Terminos y condiciones</h2>
	         <a class="cerrar" href="">X</a>
	      </div>
	      <div class="contenidoModal">
	        <p>Lorem ipsum dolor</p>
	        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
	        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
	        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
	        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
	        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
	        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
	      </div>
	      <div class="footerModal">
	        <h3>ADSI 156338 - 2018</h3>
	      </div>
	    </div>
	</div>

<!----------Footer--------->

	<footer>
		<div id='campo'>
		<p class="derechos">ADSI 156338 - 2018 Copyright © Todos los derechos reservados. </p>
			<a href="#curso1" class="button">¿Quienes Somos?</a>
			<a href="#curso1" class="button">Terminos y condiciones</a>
		</div>
		
	</footer>		
</body>
</html>