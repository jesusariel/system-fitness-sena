<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Creación de Region | System Fitness SENA</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" type="text/css" href="../../css/style.css">
	<link rel="stylesheet" type="text/css" href="../../css/estiloGlobView.css">
	<link rel="stylesheet" type="text/css" href="../../css/ubicacion/estiloUbicView.css">
	<script src="../../js/jQuery/jquery-3.3.1.js"></script>
	<script src="../../js/jQuery/jquery.min.js"></script>
	<script src="../../js/funcionMenView.js"></script>
	<script src="../../js/funcionLogiView.js"></script>
	<script src="../../js/ubicacion/funcionUbicRegiView.js"></script>
	<style>
		img{
			margin: auto;
		}
	</style>
</head>
<body>
	<header>
		<div class="logo">System Fitness SENA</div>
			<div id="menu">
				<nav >
					<ul>
					
						<li class="usuarioResponsive"><a href="#">Nombre de Usuario</a></li>
						<li><a href="../../../inicioUsuario.html" >Inicio</a></li>
						<li><a href="noticiaView.html" >Noticias</a></li>
						<li><a href="#">Inventario</a></li>
						<li><a href="#">Gimnasio</a></li>
						<li><a href="#" class="activo" >Ubicacion</a></li>
						<li><a href="#">Documentacion</a></li>
						<li class="usuarioResponsive"><a href="#">Cerrar Sesion</a></li>
							<div id="conteUser">
								<div id="btnUser">Nombre de Usuario</div>


							<form class="panelUser">
								<div id="contePanelUser">
								<div id="opcionPanel">Panel de Usuario</div>
								<div id="opcionCerrar"><a href="" id="aCerrar"><span class=''></span> Cerrar Sesion</a></div>
								</div>
							</form>
					
						</div>
					</ul>
				</nav>
			</div>
		<div class="menu-toggle">
			<div class="toggle">
				<span> </span>
				<span> </span>
				<span> </span>
			</div>
		</div>
	</header>
	<div class="menuUbicacion">
		<a href="formularioPaisView.html" class="opcionMenu ">Pais</a>
		<a href="#" class="opcionMenu seleccionado">Region</a>
		<a href="formularioDepaView.html" class="opcionMenu">Departamento</a>
		<a href="formularioMuniView.html" class="opcionMenu ">Municipios</a>
		<a href="formularioSedeView.html" class="opcionMenu">Sede</a>

</div>
<div class="contenedor-General"><!--fin de contenedor general-->

	<div class="conten">
		<form action="javascript:guardar(1,0);" method="POST" enctype="multipart/form-data" id="form">
			<div class="Titulo">
					<h2>Crear Región</h2>
					<p style="display: none;">Al no adjuntar la imagen no se cambiara</p>
				</div>
			<div class="cuatro">
				<div class="inputbox">
					<input type="text" name="txtNombreUbicacion" required>
	             	<label>Nombre:</label>
				</div>
			</div>
			<div class="cuatro">
				<div class="selectbox">
					<div class="labelSelect">
						<label>Pais</label>
					</div>
						<select name="slcPais" required>
							<?php
							include ('../../Modelo/ConexionModel.php');
							$Conexion = new Conexion();
							$Conexion->ejecutar('SELECT * FROM pais;');
							while($fila = $Conexion->obtenerObjeto()){
								echo "<option value='". $fila->id_pais ."'>". $fila->descripcion_pais ."</option>";
							}
							$Conexion->cerrarConexion();
							?>
						</select>		
				</div>
			</div>
			<div class="cuatro">

				<div class="txt-adjuntar">
					<p>Foto de Perfil:</p>
				</div>
				<div class="file">
					<p>Adjuntar De la Ubicacion</p><span class="icon icon-image"></span>
					<input type="file" name="filImagen" class="btn-imagen" required>
				</div>
			</div>
			<div class="cinco">
				<div class="selectbox">
					<div class="labelSelect">
						<label>Estado</label>
					</div>
						<select name="slcEstado" required>
							<option value="1">Activo</option>
							<option value="0">Desactivo</option>
						</select>		
				</div>
			</div>
			<!--<textarea placeholder="Descripcion" name="txtaDescripcion" required></textarea>-->
			<img src="" width="250px" style="display: none">
			<br><br>
			<div class="btn-contenedor">
			<input type="submit" id="submit" value="Agregar" class="btn-Enviar">
			<input type="button" onclick="limpiar();" value="Limpiar" class="btn-Enviar">
			</div>
		</form>
	</div>
	<div class="tituloUbicacion">Regiones Registrados</div>
	<div class="contenedorTable">
		<table>
	          <tr>
	            <td class="titulo">País</td>
	            <td class="titulo">Nombre de Region</td>
	            <td class="titulo">Acciones</td>
	          </tr>
			  <?php
				$Conexion = new Conexion();
				$Conexion->ejecutar('SELECT * FROM region INNER JOIN pais ON region.id_pais = pais.id_pais ORDER BY region.id_region;');
				while($fila = $Conexion->obtenerObjeto()){
					echo "<tr id='pos". $fila->id_region ."'>
		            <td>". $fila->descripcion_pais ."</td>
		            <td>". $fila->descripcion_region ."</td>
		            <td><input type='button' value='Editar' class='btnEditar' onclick='litarEditar(". $fila->id_region .")'><input type='button' value='Eliminar' class='btnEliminar' onclick='eliminar(". $fila->id_region .")'></td>
		          </tr>";
				}
				$Conexion->cerrarConexion();
			  ?>
		</table>
	</div>
</div><!--fin de contenedor general-->
<!----------modal--------->
	<div class="modal" id="curso1">
		<div class="contenedorModal">
	      <div class="tituloModal">
	       
	        <h2>Terminos y condiciones</h2>
	         <a class="cerrar" href="">X</a>
	      </div>
	      <div class="contenidoModal">
	        <p>Lorem ipsum dolor</p>
	        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
	        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
	        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
	        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
	        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
	        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
	      </div>
	      <div class="footerModal">
	        <h3>ADSI 156338 - 2018</h3>
	      </div>
	    </div>
	</div>

<!----------Footer--------->

	<footer>
		<div id='campo'>
		<p class="derechos">ADSI 156338 - 2018 Copyright © Todos los derechos reservados. </p>
			<a href="#curso1" class="button">¿Quienes Somos?</a>
			<a href="#curso1" class="button">Terminos y condiciones</a>
		</div>
		
	</footer>		
</body>
</html>