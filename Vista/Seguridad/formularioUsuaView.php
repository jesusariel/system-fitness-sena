<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Creación de Noticia | System Fitness SENA</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" type="text/css" href="../../css/style.css">
	<link rel="stylesheet" type="text/css" href="../../css/estiloGlobView.css">
	<script src="../../js/jQuery/jquery-3.3.1.js"></script>
	<script src="../../js/jQuery/jquery.min.js"></script>
	<script src="../../js/funcionMenView.js"></script>
	<script src="../../js/funcionLogiView.js"></script>
	<script src="../../js/funcionApreView.js"></script>
</head>
<body>
	<header>
		<div class="logo">System Fitness SENA</div>
			<div id="menu">
				<nav >
					<ul>
					
						<li class="usuarioResponsive"><a href="#">Nombre de Usuario</a></li>
						<li><a href="../../../inicioUsuario.html" >Inicio</a></li>
						<li><a href="noticiaView.html" >Noticias</a></li>
						<li><a href="#">Inventario</a></li>
						<li><a href="#">Gimnasio</a></li>
						<li><a href="#">Ubicacion</a></li>
						<li><a href="#">Documentacion</a></li>
						<li class="usuarioResponsive"><a href="#">Cerrar Sesion</a></li>
							<div id="conteUser">
								<div id="btnUser">Nombre de Usuario</div>


							<form class="panelUser">
								<div id="contePanelUser">
								<div id="opcionPanel">Panel de Usuario</div>
								<div id="opcionCerrar"><a href="" id="aCerrar"><span class=''></span> Cerrar Sesion</a></div>
								</div>
							</form>
					
						</div>
					</ul>
				</nav>
			</div>
		<div class="menu-toggle">
			<div class="toggle">
				<span> </span>
				<span> </span>
				<span> </span>
			</div>
		</div>
	</header>
<div class="contenedor-General"><!--fin de contenedor general-->

	<div class="conten">
		<div class="Titulo">
			<h2>Formulario Usuario</h2>
		</div>
	<form>	

      
		<div class="cinco">
			<div class="inputbox">
				<input type="text" id="txtPersona" value="" required="" >
             	<label>No. Persona:</label>
			</div>
		</div>
		<div class="cinco">
			<div class="inputbox">
				<input type="text" id="txtTipoUsuario" value="" required="" >
             	<label>No. Tipo Usuario:</label>
			</div>
		</div>
		<div class="cinco">
			<div class="inputbox">
				<input type="text" id="txtContrasena" value="" required="" >
             	<label>Contraseña:</label>
			</div>
		</div>
		<div class="cinco">
			<div class="inputbox">
				<input type="text" id="txtGoogle" value="" required="" >
             	<label>Google:</label>
			</div>
		</div>
		

		<div class="btn-contenedor">

				<a href=""><input type="submit" value="Actualizar" class="btn-Enviar"></a>		
		</div>

	</form>	

	</div>
	<div class="contenedorTable">
		<table>
	          <tr>
	            <td class="titulo">No. Persona</td>
	            <td class="titulo">No. Tipo Usuario</td>
	            <td class="titulo">Contraseña</td>
	           	<td class="titulo">Google</td>
	            <td class="titulo">Acciones</td>
	          </tr>
		          
		          	<?php
					require_once "../../Modelo/ConexionModel.php";

		          	$Conexion = new Conexion();
                	$sentenciaSql = "SELECT * FROM usuario";
                	$Conexion->ejecutar($sentenciaSql);
                	$result=$Conexion->obtenerRegistro();

                if ($result > 0) {
                        while($rows=$Conexion->obtenerObjeto()){
						echo'<tr>
						<td>'.$rows->id_persona.'</td>
			            <td>'.$rows->id_tipousuario.'</td>
			            <td>'.$rows->contrasena_usuario.'</td>			            		            		            
			            <td>'.$rows->google_usuario.'</td>		            
			            <td>
			            <a href="javascript: actualizar($rows->id_usuario);"><input type="submit" value="Editar" class="btnEditar"></a>
			            <input type="submit" value="Eliminar" class="btnEliminar"></a>
			            </td>
			            </tr>';	
                            }
                        } 
                     $Conexion->cerrarConexion();     
                        ?>    
		          
		</table>
	</div>
</div><!--fin de contenedor general-->
<!----------modal--------->
	<div class="modal" id="curso1">
		<div class="contenedorModal">
	      <div class="tituloModal">
	       
	        <h2>Terminos y condiciones</h2>
	         <a class="cerrar" href="">X</a>
	      </div>
	      <div class="contenidoModal">
	        <p>Lorem ipsum dolor</p>
	        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
	        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
	        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
	        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
	        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
	        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
	      </div>
	      <div class="footerModal">
	        <h3>ADSI 156338 - 2018</h3>
	      </div>
	    </div>
	</div>

<!----------Footer--------->

	<footer>
		<div id='campo'>
		<p class="derechos">ADSI 156338 - 2018 Copyright © Todos los derechos reservados. </p>
			<a href="#curso1" class="button">¿Quienes Somos?</a>
			<a href="#curso1" class="button">Terminos y condiciones</a>
		</div>
		
	</footer>		
</body>
</html>