<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Creación de Noticia | System Fitness SENA</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" type="text/css" href="../../css/style.css">
	<link rel="stylesheet" type="text/css" href="../../css/estiloGlobView.css">
	<script src="../../js/jQuery/jquery-3.3.1.js"></script>
	<script src="../../js/jQuery/jquery.min.js"></script>
	<script src="../../js/funcionMenView.js"></script>
	<script src="../../js/funcionLogiView.js"></script>
	<script src="../../js/funcionApreView.js"></script>
</head>
<body>
	<header>
		<div class="logo">System Fitness SENA</div>
			<div id="menu">
				<nav >
					<ul>
					
						<li class="usuarioResponsive"><a href="#">Nombre de Usuario</a></li>
						<li><a href="../../../inicioUsuario.html" >Inicio</a></li>
						<li><a href="noticiaView.html" >Noticias</a></li>
						<li><a href="#">Inventario</a></li>
						<li><a href="#">Gimnasio</a></li>
						<li><a href="#">Ubicacion</a></li>
						<li><a href="#">Documentacion</a></li>
						<li class="usuarioResponsive"><a href="#">Cerrar Sesion</a></li>
							<div id="conteUser">
								<div id="btnUser">Nombre de Usuario</div>


							<form class="panelUser">
								<div id="contePanelUser">
								<div id="opcionPanel">Panel de Usuario</div>
								<div id="opcionCerrar"><a href="" id="aCerrar"><span class=''></span> Cerrar Sesion</a></div>
								</div>
							</form>
					
						</div>
					</ul>
				</nav>
			</div>
		<div class="menu-toggle">
			<div class="toggle">
				<span> </span>
				<span> </span>
				<span> </span>
			</div>
		</div>
	</header>
<div class="contenedor-General"><!--fin de contenedor general-->

	<div class="conten">
		<div class="Titulo">
			<h2>Formulario Persona</h2>
		</div>
        <?php
        if (isset($_GET['persona'])) {
        $persona=$_GET['persona'];
        $nombre=$_GET['nombre'];
        $apellido=$_GET['apellido'];
        $edad=$_GET['edad'];
        $correo=$_GET['correo'];
        $eps=$_GET['eps'];
        $rh=$_GET['rh'];
    
        echo '<form action="javascript: Actualizar('.$persona.');" name="form" enctype="multipart/form-data">';
            	}
        ?>  	  
		<div class="cinco">
			<div class="inputbox">
				<input type="text" name="txtNombre" placeholder="<?php if (isset($_GET['nombre'])) {echo$nombre;} ?>" required="" >
             	<label>Nombres:</label>
			</div>
		</div>
		<div class="cinco">
			<div class="inputbox">
				<input type="text" name="txtApellido" placeholder="<?php if (isset($_GET['apellido'])) {echo $apellido;} ?>" required="" >
             	<label>Apellidos:</label>
			</div>
		</div>
		<br/>
		<div class="cinco">
			<div class="inputbox">
				<input type="text" name="txtEdad" placeholder="<?php if (isset($_GET['edad'])) { echo $edad;} ?>" required="" >
             	<label>Edad:</label>
			</div>
		</div>
		<div class="cinco">
			<div class="inputbox">
				<input type="text" name="txtCorreo" placeholder="<?php if (isset($_GET['correo'])) { echo $correo;} ?>" required="" >
             	<label>correo MISENA:</label>
			</div>
		</div>
		<br/>
		<div class="cinco">
			<div class="inputbox">
				<input type="text" name="txtEps" placeholder="<?php if (isset($_GET['eps'])) { echo $eps;} ?>" required="" >
             	<label>EPS:</label>
			</div>
		</div>
				<div class="cinco">
			<div class="inputbox">
				<input type="text" name="txtRh" placeholder="<?php if (isset($_GET['rh'])) { echo $rh;} ?>" required="" >
             	<label>RH:</label>
			</div>
		</div>
		<br/>
		<div class="cinco">
		<div class="txt-adjuntar">
				<p>Foto de Perfil:</p>
			</div>
			<div class="file">
				<p>Adjuntar Imagen</p><span class="icon icon-image"></span>
				<input type="file" name="txtArchivo" class="btn-imagen">
			</div>
		</div>

		<div class="btn-contenedor">
			<input type="hidden" name="accion" value="update">
			<input type="submit" value="Actualizar" class="btn-Enviar">		
		</div>

	</form>	

	</div>
	<div class="contenedorTable">
		<table>
	          <tr>
	            <td class="titulo">Nombre</td>
	            <td class="titulo">Apellido</td>
	            <td class="titulo">Edad</td>
	           	<td class="titulo">Correo MISENA</td>
	            <td class="titulo">EPS</td>
	            <td class="titulo">RH</td>
	            <td class="titulo">Acciones</td>
	          </tr>
		          
		          	<?php
					require_once "../../Modelo/ConexionModel.php";

		          	$Conexion = new Conexion();
                	$sentenciaSql = "SELECT * FROM persona";
                	$Conexion->ejecutar($sentenciaSql);
                	$result=$Conexion->obtenerRegistro();

                if ($result > 0) {
                        while($rows=$Conexion->obtenerObjeto()){
						echo'<tr>
						<td>'.$rows->nombre_persona.'</td>
			            <td>'.$rows->apellido_persona.'</td>
			        	<td>'.$rows->edad_persona.'</td>
			            <td>'.$rows->correo_persona.'</td>			            		            		            
			            <td>'.$rows->eps_persona.'</td>
			            <td>'.$rows->rh_persona.'</td>			            
			            <td>
			            <a href="formularioPersView.php?persona='.$rows->id_persona.'&nombre='.$rows->nombre_persona.'&apellido='.$rows->apellido_persona.'&edad='.$rows->edad_persona.'&correo='.$rows->correo_persona.'&eps='.$rows->eps_persona.'&rh='.$rows->rh_persona.'"><input type="submit" value="Editar" class="btnEditar"></a>
			            <form action="javascript: Borrar('.$rows->id_persona.');" name="form" enctype="multipart/form-data">
			            <input type="hidden" name="accion" value="delete">
			            <input type="submit" value="Eliminar" class="btnEliminar"></a>
			            </form<
			            </td>
			            </tr>';	
                            }
                        } 
                     $Conexion->cerrarConexion();     
                        ?>    
		          
		</table>
	</div>
</div><!--fin de contenedor general-->
<!----------modal--------->
	<div class="modal" id="curso1">
		<div class="contenedorModal">
	      <div class="tituloModal">
	       
	        <h2>Terminos y condiciones</h2>
	         <a class="cerrar" href="">X</a>
	      </div>
	      <div class="contenidoModal">
	        <p>Lorem ipsum dolor</p>
	        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
	        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
	        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
	        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
	        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
	        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
	      </div>
	      <div class="footerModal">
	        <h3>ADSI 156338 - 2018</h3>
	      </div>
	    </div>
	</div>

<!----------Footer--------->

	<footer>
		<div id='campo'>
		<p class="derechos">ADSI 156338 - 2018 Copyright © Todos los derechos reservados. </p>
			<a href="#curso1" class="button">¿Quienes Somos?</a>
			<a href="#curso1" class="button">Terminos y condiciones</a>
		</div>
		
	</footer>		
</body>
</html>