<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Creación de Noticia | System Fitness SENA</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" type="text/css" href="../../css/style.css">
	<link rel="stylesheet" type="text/css" href="../../css/estiloGlobView.css">
	<link rel="stylesheet" type="text/css" href="../../css/noticias/formularioNotiView.css">
	<link rel="stylesheet" type="text/css" href="../../css/noticias/css.css">
	<script src="../../js/jQuery/jquery-3.3.1.js"></script>

	<script src="../../js/funcionMenView.js"></script>
	<script src="../../js/funcionLogiView.js"></script>
  	<script src="../../js/aprendizfichaView.js"></script>
  	<script src="../../js/jsView.js"></script>
<script>
$(document).ready(function(){
  $("#buscar").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>
</head>
<body>
	<header>
		<div class="logo">System Fitness SENA</div>
			<div id="menu">
				<nav >
					<ul>
					
						<li class="usuarioResponsive"><a href="#">Nombre de Usuario</a></li>
						<li><a href="../../../inicioUsuario.html" >Inicio</a></li>
						<li><a href="noticiaView.html" class="activo">Noticias</a></li>
						<li><a href="#">Inventario</a></li>
						<li><a href="#">Gimnasio</a></li>
						<li><a href="#">Ubicacion</a></li>
						<li><a href="#">Documentacion</a></li>
						<li class="usuarioResponsive"><a href="#">Cerrar Sesion</a></li>
							<div id="conteUser">
								<div id="btnUser">Nombre de Usuario</div>


							<form class="panelUser">
								<div id="contePanelUser">
								<div id="opcionPanel">Panel de Usuario</div>
								<div id="opcionCerrar"><a href="" id="aCerrar"><span class=''></span> Cerrar Sesion</a></div>
								</div>
							</form>
					
						</div>
					</ul>
				</nav>
			</div>
		<div class="menu-toggle">
			<div class="toggle">
				<span> </span>
				<span> </span>
				<span> </span>
			</div>
		</div>
	</header>
	<div class="contenedor-General"><!--fin de contenedor general-->
	<div class="contenedor">
		<div class="frm-contenedor">
			<div class="Titulo">
			
			</div>

			
				<form id="form">
                <h2>Aprendices Ficha</h2>
					<div class="completo">
						<div class="inputbox">

							<input id="txtficha" min="1"  type="number" name="txtficha" required="" >
			             	<label></span>Ficha: </label>
						</div>
					</div>
<input id="buscar" placeholder="buscar">
						<h2>Aprendices</h2>
	<div class="contenedorTable">

	<table>
	          <tr>
	            <td class="titulo">DOCUMENTO</td>
	            <td class="titulo">NOMBRE</td>
                 <td class="titulo">APELLIDO</td>
         <td class="titulo">FICHA</td>
	            <td class="titulo">Acciones</td>
	          </tr>

 <tbody id="myTable">
		   


    <?php
   include "../../Modelo/ConexionModel.php";
     $Conexion= new Conexion();
  $sql = " SELECT * FROM aprendiz,persona,aprendizficha,ficha WHERE persona.id_persona=aprendiz.id_persona AND ficha.id_ficha=aprendizficha.id_ficha AND aprendiz.id_aprendiz=aprendizficha.id_aprendiz ";

$Conexion->ejecutar($sql);
        while($ress = $Conexion->obtenerObjeto()){


       
    $aprendiz=$ress->id_aprendiz;
  
    ?>

	         <tr>
  
             
		            <td><?php echo $ress->documento_persona; ?></td>
		            <td><?php echo $ress->nombre_persona; ?></td>
                    <td><?php echo $ress->apellido_persona; ?></td>




<td><?php echo $ress->ficha_ficha ?></td>


		            <td><input type="checkbox" id="s" onclick=" seleccionar(<?php echo  $ress->id_aprendiz;?>); "  /> </td>
		       


</tr>
<?php

}
  
?>




   </tbody>
   





		</table>
	</div>



            </div>
	</div>
</form>
	<br>





</div><!--fin de contenedor general-->
	<footer>
		<div id='campo'>
		<p class="derechos">ADSI 156338 - 2018 Copyright © Todos los derechos reservados. </p>
			<button id="modal-btn" class="button">¿Quienes Somos?</button>
			<button id="modal-btn" class="button">Terminos y condiciones</button>
		</div>
		
	</footer>	







<script>


function autocomplete(inp, arr) {
  /*the autocomplete function takes two arguments,
  the text field element and an array of possible autocompleted values:*/
  var currentFocus;
  /*execute a function when someone writes in the text field:*/
  inp.addEventListener("input", function(e) {
      var a, b, i, val = this.value;
      /*close any already open lists of autocompleted values*/
      closeAllLists();
      if (!val) { return false;}
      currentFocus = -1;
      /*create a DIV element that will contain the items (values):*/
      a = document.createElement("DIV");
      a.setAttribute("id", this.id + "autocomplete-list");
      a.setAttribute("class", "autocomplete-items");
      /*append the DIV element as a child of the autocomplete container:*/
      this.parentNode.appendChild(a);
      /*for each item in the array...*/
      for (i = 0; i < arr.length; i++) {
        /*check if the item starts with the same letters as the text field value:*/
        if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
          /*create a DIV element for each matching element:*/
          b = document.createElement("DIV");
          /*make the matching letters bold:*/
          b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
          b.innerHTML += arr[i].substr(val.length);
          /*insert a input field that will hold the current array item's value:*/
          b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
          /*execute a function when someone clicks on the item value (DIV element):*/
          b.addEventListener("click", function(e) {
              /*insert the value for the autocomplete text field:*/
              inp.value = this.getElementsByTagName("input")[0].value;
              /*close the list of autocompleted values,
              (or any other open lists of autocompleted values:*/
              closeAllLists();
          });
          a.appendChild(b);
        }
      }
  });
  /*execute a function presses a key on the keyboard:*/
  inp.addEventListener("keydown", function(e) {
      var x = document.getElementById(this.id + "autocomplete-list");
      if (x) x = x.getElementsByTagName("div");
      if (e.keyCode == 40) {
        /*If the arrow DOWN key is pressed,
        increase the currentFocus variable:*/
        currentFocus++;
        /*and and make the current item more visible:*/
        addActive(x);
      } else if (e.keyCode == 38) { //up
        /*If the arrow UP key is pressed,
        decrease the currentFocus variable:*/
        currentFocus--;
        /*and and make the current item more visible:*/
        addActive(x);
      } else if (e.keyCode == 13) {
        /*If the ENTER key is pressed, prevent the form from being submitted,*/
        e.preventDefault();
        if (currentFocus > -1) {
          /*and simulate a click on the "active" item:*/
          if (x) x[currentFocus].click();
        }
      }
  });
  function addActive(x) {
    /*a function to classify an item as "active":*/
    if (!x) return false;
    /*start by removing the "active" class on all items:*/
    removeActive(x);
    if (currentFocus >= x.length) currentFocus = 0;
    if (currentFocus < 0) currentFocus = (x.length - 1);
    /*add class "autocomplete-active":*/
    x[currentFocus].classList.add("autocomplete-active");
  }
  function removeActive(x) {
    /*a function to remove the "active" class from all autocomplete items:*/
    for (var i = 0; i < x.length; i++) {
      x[i].classList.remove("autocomplete-active");
    }
  }
  function closeAllLists(elmnt) {
    /*close all autocomplete lists in the document,
    except the one passed as an argument:*/
    var x = document.getElementsByClassName("autocomplete-items");
    for (var i = 0; i < x.length; i++) {
      if (elmnt != x[i] && elmnt != inp) {
        x[i].parentNode.removeChild(x[i]);
      }
    }
  }
  /*execute a function when someone clicks in the document:*/
  document.addEventListener("click", function (e) {
      closeAllLists(e.target);
  });
}

/*An array containing all the country names in the world:*/

var countries = [
<?php


     $sql = "SELECT * FROM ficha"; 

$Conexion->ejecutar($sql);
        while($ress = $Conexion->obtenerObjeto()){


   echo '"'.$ress->ficha_ficha.'",';
        	}
        	?>
]


/*initiate the autocomplete function on the "myInput" element, and pass along the countries array as possible autocomplete values:*/
autocomplete(document.getElementById("txtficha"), countries);
</script>



 




</body>
</html>