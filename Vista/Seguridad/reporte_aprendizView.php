<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <title>Datos Aprendiz</title>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../../css/estiloDataTableStandarView.css">
    <link rel="stylesheet" type="text/css" href="../../css/dataTableView.css">
    <!--<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">-->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="../../js/funcionMenView.js"></script>
    <script src="../../js/funcionLogiView.js"></script>
    <!--freiman-->
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script src="../../js/Data_Table.js"></script>

    <style type="text/css">
        #dataTable_filter>label {
            float: right;
        }
    </style>
</head>

<body>
    <header>
        <div class="logo">System Fitness SENA</div>
        <div id="menu">
            <nav>
                <ul>
                    <li class="usuarioResponsive"><a href="#">Nombre de Usuario</a></li>
                    <li><a href="#" class="activo">Inicio</a></li>
                    <li><a href="#">Noticias</a></li>
                    <li><a href="#">Inventario</a></li>
                    <li><a href="#">Gimnasio</a></li>
                    <li><a href="#">Ubicacion</a></li>
                    <li><a href="#">Documentacion</a></li>
                    <li class="usuarioResponsive">
                        <a href="#"></a>
                    </li>
                    <div id="conteUser">
                        <div id="btnUser">Nombre de Usuario</div>

                        <form class="panelUser">
                            <div id="contePanelUser">
                                <div id="opcionPanel">Panel de Usuario</div>
                                <div id="opcionCerrar"><a href="" id="aCerrar"><span class=''></span> Cerrar Sesion</a></div>
                            </div>
                        </form>

                    </div>
                </ul>

            </nav>
        </div>
        <div class="menu-toggle">
            <div class="toggle">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
    </header>
    <br>
    <div class="contenedor-General fondoVisible">
        <!--Inicio de contenedor general-->

        <table id="dataTable">
            <thead class="titulo">
                <tr>
                    <td>Nombre</td>
                    <td>Identificacion</td>
                    <td>Direccion</td>
                    <td>Telefono</td>
                    <td>E-MAIL</td>
                </tr>
            </thead>
            <tbody>

                <?php 

                require_once ('../../Controlador/connect_database.php');

                while ($res= $rs ->fetch_array(MYSQLI_BOTH)) {
                    echo " 
                        <tr>

                            <th>".$res['primer_nombre']." ".$res['segundo_nombre']." ".$res['primer_apellido']."</th>
                            <th>".$res['identificacion']."</th>
                            <th>".$res['direccion']."</th>
                            <th>".$res['telefono']."</th>
                            <th>".$res['email']."</th>

                        </tr>";
	           }

            ?>

            </tbody>
            <tfoot>
                <tr>
                    <th>Nombre</th>
                    <th>Identificacion</th>
                    <th>Direccion</th>
                    <th>Telefono</th>
                    <th>E-MAIL</th>

                </tr>
            </tfoot>
        </table>

    </div>
    <!--fin de contenedor general-->

    <!----------modal--------->
    <div class="modal" id="curso1">
        <div class="contenedorModal">
            <div class="tituloModal">

                <h2>Terminos y condiciones</h2>
                <a class="cerrar" href="">X</a>
            </div>
            <div class="contenidoModal">
                <p>Lorem ipsum dolor</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            </div>
            <div class="footerModal">
                <h3>ADSI 156338 - 2018</h3>
            </div>
        </div>
    </div>

    <!----------Footer--------->

    <footer>
        <div id='campo'>
            <p class="derechos">ADSI 156338 - 2018 Copyright © Todos los derechos reservados. </p>
            <a href="#curso1" class="button">¿Quienes Somos?</a>
            <a href="#curso1" class="button">Terminos y condiciones</a>
        </div>

    </footer>

</body>

</html>