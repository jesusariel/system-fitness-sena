<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Creación de Profesional | System Fitness SENA</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" type="text/css" href="../../css/style.css">
	<link rel="stylesheet" type="text/css" href="../../css/estiloGlobView.css">
	<link rel="stylesheet" type="text/css" href="../../css/Seguridad/estiloProfView.css">
	<script src="../../js/jQuery/jquery-3.3.1.js"></script>
	<script src="../../js/jQuery/jquery.min.js"></script>
	<script src="../../js/funcionMenView.js"></script>
	<script src="../../js/funcionLogiView.js"></script>
	<script src="../../js/Seguridad/profesionalView.js"></script>
</head>
<body>
	<header>
		<div class="logo">System Fitness SENA</div>
			<div id="menu">
				<nav >
					<ul>
					
						<li class="usuarioResponsive"><a href="#">Nombre de Usuario</a></li>
						<li><a href="../../../inicioUsuario.html" >Inicio</a></li>
						<li><a href="noticiaView.html" >Noticias</a></li>
						<li><a href="#">Inventario</a></li>
						<li><a href="#">Gimnasio</a></li>
						<li><a href="#" class="activo" >Ubicacion</a></li>
						<li><a href="#">Documentacion</a></li>
						<li class="usuarioResponsive"><a href="#">Cerrar Sesion</a></li>
							<div id="conteUser">
								<div id="btnUser">Nombre de Usuario</div>


							<form class="panelUser">
								<div id="contePanelUser">
								<div id="opcionPanel">Panel de Usuario</div>
								<div id="opcionCerrar"><a href="" id="aCerrar"><span class=''></span> Cerrar Sesion</a></div>
								</div>
							</form>
					
						</div>
					</ul>
				</nav>
			</div>
		<div class="menu-toggle">
			<div class="toggle">
				<span> </span>
				<span> </span>
				<span> </span>
			</div>
		</div>
	</header>
	
<div class="contenedor-General"><!--fin de contenedor general-->
<br>
<br>
<br>
	<div class="conten">
		<form action="javascript: guardar(1,1)" id="form">
            <div class="Titulo">
                    <h2>Crear Profesional</h2>
                </div>
            <div class="cinco">
                <div class="inputbox">
                    <input type="text" id="txtNombre" name="txtNombre" required="" pattern="[A-Z a-z]+" maxlength="30">
                    <label >Nombre:</label>
                </div>
            </div>
            <div class="cinco">
                <div class="inputbox">
                    <input type="number" id="numbrNumeroDoc" name="numbrNumeroDoc" required="" pattern="[0-9]+" min="0" max="2000000000">
                    <label >Numero Documento:</label>
                </div>
            </div>
            <div class="cinco">
                <div class="selectbox">
                    <div class="labelSelect">
                        <label for="subdirector">Tipo</label>
                    </div>
                        <select id="slcTipo" name="slcTipo">
                            <option value="GYM">GYM</option>
                            <option value="Alimentacion">Alimentacion</option>
                            <option value="Bienestar">Bienestar</option>

                        </select>		
                </div>
            </div>
             <div class="cinco">
                <div class="inputbox">
                    <input type="number" id="numbrTelefono" name="numbrTelefono" required="" pattern="[0-9]+" min="0" max="4000000000">
                    <label >Telefono:</label>
                </div>
            </div>
            <div class="cinco">
                <div class="inputbox">
                    <input type="email" id="txtCorreo" name="txtCorreo" required="" maxlength="30">
                    <label >Correo:</label>
                </div>
            </div>
            <br><br>
            <input type="submit" name="Agregar" id="submit" value="Agregar" class="btn-Enviar">		
            </div>
		</form>
	</div>
	<div class="tituloUbicacion">Profesionales Registradas</div>
	<div class="contenedorTable">
		<table>
	          <tr>
	            <td class="titulo">Nombre</td>
	            <td class="titulo">Numero Doc</td>
	            <td class="titulo">Tipo Profesional</td>
	            <td class="titulo">Telefono</td>
                <td class="titulo">Correo</td>
                <td class="titulo">Accion</td>
	          </tr>
			  <?php
                include ('../../Modelo/Seguridad/conexionModel.php');
                $Conexion = new Conexion();
				$Conexion->ejecutar('SELECT * FROM profesional ORDER BY id_profesional;');
				while($fila = $Conexion->obtenerObjeto()){
					echo "<tr id='pos". $fila->id_profesional ."'>
		            <td>". $fila->nombre_profesional ."</td>
		            <td>". $fila->numero_documento_profesional ."</td>
		            <td>". $fila->tipo_profesional ."</td>
                    <td>". $fila->telefono_profesional ."</td>
                    <td>". $fila->correo_profesional ."</td>
		            <td><input type='button' value='Editar' class='btnEditar' onclick='litarEditar(". $fila->id_profesional .")'><input type='button' value='Eliminar' class='btnEliminar' onclick='eliminar(". $fila->id_profesional .")'></td>
		          </tr>";
				}
				$Conexion->cerrarConexion();
			  ?>
		</table>
	</div>
</div><!--fin de contenedor general-->
<!----------modal--------->
	<div class="modal" id="curso1">
		<div class="contenedorModal">
	      <div class="tituloModal">
	       
	        <h2>Terminos y condiciones</h2>
	         <a class="cerrar" href="">X</a>
	      </div>
	      <div class="contenidoModal">
	        <p>Lorem ipsum dolor</p>
	        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
	        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
	        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
	        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
	        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
	        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
	      </div>
	      <div class="footerModal">
	        <h3>ADSI 156338 - 2018</h3>
	      </div>
	    </div>
	</div>

<!----------Footer--------->

	<footer>
		<div id='campo'>
		<p class="derechos">ADSI 156338 - 2018 Copyright © Todos los derechos reservados. </p>
			<a href="#curso1" class="button">¿Quienes Somos?</a>
			<a href="#curso1" class="button">Terminos y condiciones</a>
		</div>
		
	</footer>		
</body>
</html>