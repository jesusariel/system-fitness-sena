<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Creación de Instructor | System Fitness SENA</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" type="text/css" href="../../css/style.css">
	<link rel="stylesheet" type="text/css" href="../../css/estiloGlobView.css">
	<link rel="stylesheet" type="text/css" href="../../css/ubicacion/estiloUbicView.css">
	<script src="../../js/jQuery/jquery-3.3.1.js"></script>
	<script src="../../js/jQuery/jquery.min.js"></script>
	<script src="../../js/funcionMenView.js"></script>
	<script src="../../js/instructorView.js"></script>
</head>
<body>
	<header>
		<div class="logo">System Fitness SENA</div>
			<div id="menu">
				<nav >
					<ul>
					
						<li class="usuarioResponsive"><a href="#">Nombre d Usuario</a></li>
						<li><a href="../../../inicioUsuario.html" >Inicio</a></li>
						<li><a href="noticiaView.html" >Noticias</a></li>
						<li><a href="#">Inventario</a></li>
						<li><a href="#">Gimnasio</a></li>
						<li><a href="#" class="activo" >Ubicacion</a></li>
						<li><a href="#">Documentacion</a></li>
						<li class="usuarioResponsive"><a href="#">Cerrar Sesion</a></li>
							<div id="conteUser">
								<div id="btnUser">Nombre de Usuario</div>


						
					
						</div>
					</ul>
				</nav>
			</div>
		<div class="menu-toggle">
			<div class="toggle">
				<span> </span>
				<span> </span>
				<span> </span>
			</div>
		</div>
	</header>

	<script type="text/javascript">
		function reset(){
			document.getElementById("form").reset();
		}

	</script>
<div class="contenedor-General"><!--fin de contenedor general-->

	<div id="ventana">	

	<div class="conten">
		<div class="Titulo">
<br><br>
				<h2>Crear Instructor</h2>
			</div>
		<div class="cinco">
			<div class="inputbox">
				<form id="form">
				<input type="text" id="txtnombre" placeholder="Nombre" maxlength="50"  value="" required="" >
          
			</div>
		</div>
		<div class="cinco">
			<div class="inputbox">
				<input type="text" id="txtapellido" placeholder="Apellido" maxlength="50" value="" required="" >
             
			</div>
		</div>
		<div class="cinco">
			<div class="inputbox">
				<input type="number" placeholder="Edad" min="1" max="150" id="txtedad" value="" required="" >
             
			</div>
		</div>
		<div class="cinco">
			<div class="inputbox">
				<input type="email" placeholder="Email" id="txtcorreo" maxlength="100"  value="" required="" >
             
			</div>
		</div>
			<div class="cinco">
			<div class="inputbox">
				<input type="password" minlength="8" placeholder="Contraseña" id="txtcontrasena"  value="" required="" >
           
			</div>
		</div>
		<div class="cinco">

			<div class="inputbox">
				<input type="number" placeholder="Documento de Id"  id="txtdocumento" maxlength="11" min="1" value="" required="" >
             
			</div>
		</div>
		<div class="cinco">
			<div class="inputbox">
				<input type="text" placeholder="RH" id="txtrh" maxlength="2" value="" required="" >
             
			</div>
		</div>
			<div class="cinco">
			<div class="inputbox">
				<input type="text" placeholder="EPS" id="txteps" maxlength="20" value="" required="" >
             	
			</div>
		</div>
		<br><br>
		<div class="btn-contenedor">
					<input  type="submit"  onclick=" agregar(); " value="Agregar" class="btn-Enviar">
					<input  type="submit"  onclick=" reset(); " value="Limpiar" class="btn-Enviar">
					</form>		
		</div>
	</div>
</div>
<div id="resultado">
	<div class="tituloUbicacion">Instructores Registrados</div>
	<div class="contenedorTable">
		<table>
	          <tr>
	            <td class="titulo">Nombre</td>
	            <td class="titulo">Apellido</td>
	            <td class="titulo">Edad</td>
                      <td class="titulo">Domunto de Identificacion</td>
                       <td class="titulo">EPS</td>
          
                 <td class="titulo">RH</td>
                      <td class="titulo">Acciones</td>
	          </tr>

	          <?php

   include "../../Modelo/ConexionModel.php";
   $Conexion= new Conexion();
     $sql = "SELECT * FROM persona,instructor WHERE persona.id_persona=instructor.id_persona "; 

$Conexion->ejecutar($sql);
        while($ress = $Conexion->obtenerObjeto()){

        	
        	?>
		          <tr>
		            <td><?php echo $ress->nombre_persona; ?></td>
		            <td><?php echo $ress->apellido_persona; ?></td>
                    <td><?php echo $ress->edad_persona; ?></td>
                    <td><?php echo $ress->documento_persona; ?></td>
                    <td><?php echo $ress->eps_persona; ?></td>
                    <td><?php echo $ress->rh_persona; ?></td>
		            <td><input type="submit" onclick=" editar(<?php echo $ress->id_persona ?>); " name="" value="Editar"  class="btnEditar">                                                                          <input type="submit" name="" onclick=" eliminar(<?php echo $ress->id_persona ?>); " value="Eliminar" class="btnEliminar"></td>
		          </tr>
<?php
}
?>

		</table>
	</div>
</div>
</div><!--fin de contenedor general-->
<!----------modal--------->
	<div class="modal" id="curso1">
		<div class="contenedorModal">
	      <div class="tituloModal">
	       
	        <h2>Terminos y condiciones</h2>
	         <a class="cerrar" href="">X</a>
	      </div>
	      <div class="contenidoModal">
	        <p>Lorem ipsum dolor</p>
	        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
	        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
	        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
	        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
	        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
	        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
	      </div>
	      <div class="footerModal">
	        <h3>ADSI 156338 - 2018</h3>
	      </div>
	    </div>
	</div>

<!----------Footer--------->

	<footer>
		<div id='campo'>
		<p class="derechos">ADSI 156338 - 2018 Copyright © Todos los derechos reservados. </p>
			<a href="#curso1" class="button">¿Quienes Somos?</a>
			<a href="#curso1" class="button">Terminos y condiciones</a>
		</div>
		
	</footer>		
</body>
</html>