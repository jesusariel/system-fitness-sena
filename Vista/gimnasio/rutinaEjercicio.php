<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Ejemplo de Estilo Global</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" type="text/css" href="../../css/style.css">
	<link rel="stylesheet" type="text/css" href="../../css/estiloGlobView.css">
	<link rel="stylesheet" type="text/css" href="../../css/gimnasio/rutinasEjercicios.css">
	<script src="../../js/jQuery/jquery-3.3.1.js"></script>
	<script src="../../js/jQuery/jquery.min.js"></script>
	<script src="../../js/funcionMenView.js"></script>
	<script src="../../js/funcionLogiView.js"></script>
	<script src="../../js/funcionRutina.js"></script>
</head>
<body>
	<header>
		<div class="logo">System Fitness SENA</div>
			<div id="menu">
				<nav >
					<ul>
						<li class="usuarioResponsive"><a href="#">Nombre de Usuario</a></li>
						<li><a href="../../../inicioUsuario.html" >Inicio</a></li>
						<li><a href="../noticia/noticiaView.html">Noticias</a></li>
						<li><a href="#">Inventario</a></li>
						<li><a href="#" class="activo">Gimnasio</a></li>
						<li><a href="#">Ubicacion</a></li>
						<li><a href="#">Documentacion</a></li>
						<li class="usuarioResponsive"><a href="#">Cerrar Sesion</a></li>
							<div id="conteUser">
								<div id="btnUser">Nombre de Usuario</div>
							<form class="panelUser">
								<div id="contePanelUser">
								<div id="opcionPanel">Panel de Usuario</div>
								<div id="opcionCerrar"><a href="" id="aCerrar"><span class=''></span> Cerrar Sesion</a></div>
								</div>
							</form>
					
						</div>
					</ul>
				</nav>
			</div>
		<div class="menu-toggle">
			<div class="toggle">
				<span></span>
				<span></span>
				<span></span>
			</div>
		</div>
	</header>
	<div class="contenedor-General" ><!--fin de contenedor general-->
	<div class="contenedor">
			<div class="Titulo">
				<h2>Ejercicio Rutina</h2>
			</div>
			<form>
				<div class="tres">
					<div class="selectbox">
						<div class="labelSelect">
							<label>Tipo de Rutina:</label>
						</div>
							<select id="id_rutina" >

								<option value="1" >Rutina A</option>
								<option value="2" >Rutina B</option>

							</select>		
					</div>
				</div>
				<br>
				<div class="tres">
					<div class="selectbox">
						<div class="labelSelect">
							<label>Ejercicios:</label>
						</div>
							<select id="id_ejercicio">
								<option value="1">Ejercicio A</option>
								<option value="2">Ejercicio B</option>
							</select>		
					</div>
				</div>
				<br>
				<br>
						<div class="btn-contenedor" >
			
			
			<button onclick="registrar();" type="button" class="btn-Enviar">Agregar</button>
				
			
		</div>

	</div>
</div>





	</div>



   
    
	


				
			</form>
		</div>

	
	<br>
	<br>
</div><!--fin de contenedor general-->
	<footer>
		<div id='campo'>
		<p class="derechos">ADSI 156338 - 2018 Copyright © Todos los derechos reservados. </p>
			<button id="modal-btn" class="button">¿Quienes Somos?</button>
			<button id="modal-btn" class="button">Terminos y condiciones</button>
		</div>
		
	</footer>		
</body>
</html>  