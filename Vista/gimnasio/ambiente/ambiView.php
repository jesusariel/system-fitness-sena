<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>System Fitness SENA - Ambiente</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" type="text/css" href="../../../css/style.css">
	<link rel="stylesheet" type="text/css" href="../../../css/estiloGlobView.css">
	<link rel="stylesheet" type="text/css" href="../../../css/Seguridad/nivelprog/formularioGimnView.css">
	<link rel="stylesheet" type="text/css" href="../../../css/Seguridad/nivelprog/gymFormularioView.css">
	<script src="../../../js/jQuery/jquery-3.3.1.js"></script>
	<script src="../../../js/jQuery/jquery.min.js"></script>
	<script src="../../../js/funcionMenView.js"></script>
	<script src="../../../js/funcionLogiView.js"></script>
	<script src="../../../js/Seguridad/niveProgView.js"></script>
</head>
<body>
	<header>
		<div class="logo">System Fitness SENA</div>
			<div id="menu">
				<nav >
					<ul>
						<li class="usuarioResponsive"><a href="#">Nombre de Usuario</a></li>
						<li><a href="../../../inicioUsuario.html" >Inicio</a></li>
						<li><a href="../noticia/noticiaView.html">Noticias</a></li>
						<li><a href="#">Inventario</a></li>
						<li><a href="·" class="activo">Gimnasio</a></li>
						<li><a href="#">Ubicacion</a></li>
						<li><a href="#">Documentacion</a></li>
						<li class="usuarioResponsive"><a href="#">Cerrar Sesion</a></li>
							<div id="conteUser">
								<div id="btnUser">Nombre de Usuario</div>


							<form class="panelUser" id="">
								<div id="contePanelUser">
								<div id="opcionPanel">Panel de Usuario</div>
								<div id="opcionCerrar"><a href="" id="aCerrar"><span class=''></span> Cerrar Sesion</a></div>
								</div>
							</form>
					
						</div>
					</ul>
				</nav>
			</div>
		<div class="menu-toggle">
			<div class="toggle">
				<span></span>
				<span></span>
				<span></span>
			</div>
		</div>
	</header>
	<div class="contenedor-General"><!--fin de contenedor general-->
	<div class="contenedor">
			<div class="Titulo">
				<h2>Ambiente </h2>
			</div>
			<form action="javascript: guardar();">

				<div class="tres">
					<div class="inputbox">
						<input type="number"  name="txtPrograma" id="txtPrograma" value="" onkeypress="return valida(event)" max="4294967296" required="" >
		             	<label>Nº Codigo</label>
					</div>
				</div>



				<textarea placeholder="Descripcion"></textarea>	

				
				<br>
				<br>
					<br>
					<input type="submit" name="Agregar" id="Agregar" value="Agregar" class="btn-Enviar">
					
					<input type="reset" name="" value="Cancelar" class="btn-Enviar">
				
			</form>
		</div>

		<div class="contenedorTable">
		<table>
			<thead >
	          <tr>
	            <td class="titulo">N° codigo</td>
	            <td class="titulo">Descripcion</td>
	          </tr>
	          </thead>
		          <tbody>
		          	<?php 
		          		require_once('../../../Modelo/ConexionModel.php');
		          		$Conexion = new Conexion();
		          		$sql="SELECT * FROM ambiente";
		          		$Conexion->ejecutar($sql);
		          		while ($ress=$Conexion->obtenerObjeto()) {
		          			 

		          				echo"<tr id='pos". $ress->id_ambiente ."'>
		          				<td>".$ress->id_ambiente."</td>
		          				<td>".$ress->descripcion_ambiente."</td>
		          			";
		   
			          	?>

	<td><input type="submit" name="" value="Editar"class="btnEditar" onclick="editar(<?php echo $ress->programa?>)"><input type="submit" name="" value="Eliminar" onclick="eliminar(<?php echo $ress->programa?>)" class="btnEliminar"></td>
			          </tr>
			          <?php
			          } 
			          ?>

			      </tbody>
	    </table>
	</div>


		
	<br>
	<br>
</div><!--fin de contenedor general-->

<!----------modal--------->
	<div class="modal" id="curso1">
		<div class="contenedorModal">
	      <div class="tituloModal">
	       
	        <h2>Terminos y condiciones</h2>
	         <a class="cerrar" href="">X</a>
	      </div>
	      <div class="contenidoModal">
	        <p>Lorem ipsum dolor</p>
	        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
	        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
	        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
	        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
	        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
	        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
	      </div>
	      <div class="footerModal">
	        <h3>ADSI 156338 - 2018</h3>
	      </div>
	    </div>
	</div>

<!----------Footer--------->

	<footer>
		<div id='campo'>
		<p class="derechos">ADSI 156338 - 2018 Copyright © Todos los derechos reservados. </p>
			<a href="#curso1" class="button">¿Quienes Somos?</a>
			<a href="#curso1" class="button">Terminos y condiciones</a>
		</div>
		
	</footer>		
</body>
</html>