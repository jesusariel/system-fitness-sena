-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 10-12-2018 a las 03:13:56
-- Versión del servidor: 10.1.36-MariaDB
-- Versión de PHP: 5.6.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `fitness_sena`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `actividad`
--

CREATE TABLE `actividad` (
  `id_actividad` int(11) NOT NULL,
  `id_gimnasio` int(11) NOT NULL,
  `id_tipo` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `estado` bit(1) NOT NULL,
  `id_usuario_creacion` int(11) NOT NULL,
  `id_usuario_modificacion` int(11) NOT NULL,
  `fecha_creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fecha_modificacion` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ambiente`
--

CREATE TABLE `ambiente` (
  `id_ambiente` int(11) NOT NULL,
  `descripcion_ambiente` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ambiente`
--

INSERT INTO `ambiente` (`id_ambiente`, `descripcion_ambiente`) VALUES
(1, 'saasd');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aprendiz`
--

CREATE TABLE `aprendiz` (
  `id_aprendiz` int(11) NOT NULL,
  `estado_aprendiz` enum('Activo','Inactivo') NOT NULL,
  `id_persona` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `aprendiz`
--

INSERT INTO `aprendiz` (`id_aprendiz`, `estado_aprendiz`, `id_persona`) VALUES
(1, 'Activo', 1),
(2, 'Activo', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aprendizficha`
--

CREATE TABLE `aprendizficha` (
  `id_aprendizficha` int(11) NOT NULL,
  `id_aprendiz` int(11) NOT NULL,
  `id_ficha` int(11) NOT NULL,
  `estado_aprendizficha` enum('Activo','Inactivo') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `aprendizficha`
--

INSERT INTO `aprendizficha` (`id_aprendizficha`, `id_aprendiz`, `id_ficha`, `estado_aprendizficha`) VALUES
(1, 2, 5, 'Activo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aprendiz_ficha`
--

CREATE TABLE `aprendiz_ficha` (
  `id_aprendiz_fichca` int(11) NOT NULL,
  `id_ficha` int(11) NOT NULL,
  `id_aprendiz` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aseguradora`
--

CREATE TABLE `aseguradora` (
  `id_aseguradora` int(11) NOT NULL,
  `nit` int(11) NOT NULL,
  `razon_social` varchar(50) NOT NULL,
  `direccion` varchar(40) NOT NULL,
  `representante_legal` varchar(50) NOT NULL,
  `id_usuario_creacion` int(11) NOT NULL,
  `id_usuario_modificacion` int(11) NOT NULL,
  `fecha_creacion` date NOT NULL,
  `fecha_modificacion` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `centro_formacion`
--

CREATE TABLE `centro_formacion` (
  `id_centro_formacion` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `id_aseguradora` int(11) NOT NULL,
  `id_regional` int(11) NOT NULL,
  `id_ciudad` int(11) NOT NULL,
  `id_usuario_creacion` int(11) NOT NULL,
  `id_usuario_modificacion` int(11) NOT NULL,
  `fecha_creacion` date NOT NULL,
  `fecha_modificacion` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `chequeoinventario`
--

CREATE TABLE `chequeoinventario` (
  `id_chequeoinventario` int(11) NOT NULL,
  `codigo_chequeoinventario` int(11) NOT NULL,
  `id_inventariogym` int(11) NOT NULL,
  `cantidad_chequeoinventario` int(11) NOT NULL,
  `id_estadoproducto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `chequeonovedad`
--

CREATE TABLE `chequeonovedad` (
  `id_chequeonovedad` int(11) NOT NULL,
  `codigo_chequeoinventario` int(11) NOT NULL,
  `novedad_chequeonovedad` varchar(1000) NOT NULL,
  `fechacreacion_chequeoinventario` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fechamodificacion_chequeoinventario` datetime DEFAULT NULL,
  `usuariocreacion_chequeoinventario` varchar(20) NOT NULL,
  `usuariomodificacion_chequeoinventario` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciudad`
--

CREATE TABLE `ciudad` (
  `id_ciudad` int(11) NOT NULL,
  `codigo` int(4) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `id_departamento` int(11) NOT NULL,
  `id_usuario_creacion` int(11) NOT NULL,
  `id_usuario_modificacion` int(11) NOT NULL,
  `fecha_creacion` date NOT NULL,
  `fecha_modificacion` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departamento`
--

CREATE TABLE `departamento` (
  `id_departamento` int(11) NOT NULL,
  `descripcion_departamento` varchar(40) NOT NULL,
  `id_region` int(11) NOT NULL,
  `fechacreacion_departamento` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fechamodificacion_departamento` datetime DEFAULT NULL,
  `usuariocreacion_departamento` varchar(20) NOT NULL,
  `usuariomodificacion_departamento` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `departamento`
--

INSERT INTO `departamento` (`id_departamento`, `descripcion_departamento`, `id_region`, `fechacreacion_departamento`, `fechamodificacion_departamento`, `usuariocreacion_departamento`, `usuariomodificacion_departamento`) VALUES
(1, 'Atlàntico', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(2, 'Bolivar', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(3, 'Cesar', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(4, 'Còrdoba', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(5, 'La Guajira', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(6, 'Magdalena', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(7, 'San Andrès y Providencia', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(8, 'Sucre', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(9, 'Antoquia', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(10, 'Boyacà', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(11, 'Caldas', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(12, 'Cundinamarca', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(13, 'Huila', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(14, 'Norte de Santader', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(15, 'Quindio', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(16, 'Risaralda', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(17, 'Santader', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(18, 'Tolima', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(19, 'Cauca', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(20, 'Chocò', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(21, 'Nariño', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(22, 'Valle de Cauca', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(23, 'Arauca', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(24, 'Casanare', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(25, 'Meta', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(26, 'Vichada', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(27, 'Amazonas', 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(28, 'Caquetà', 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(29, 'Guainìa', 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(30, 'Guaviare', 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(31, 'Putumayo', 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(32, 'Vaupès', 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `eps`
--

CREATE TABLE `eps` (
  `id_eps` int(11) NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `estado` bit(1) NOT NULL,
  `id_usuario_creacion` int(11) NOT NULL,
  `id_usuario_modificacion` int(11) NOT NULL,
  `fecha_creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fecha_modificacion` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `equipo`
--

CREATE TABLE `equipo` (
  `id_equipo` int(11) NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `cantidad` int(11) NOT NULL,
  `serial` int(11) NOT NULL,
  `descripcion` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `fecha_ingreso` date NOT NULL,
  `id_gimnasio` int(11) NOT NULL,
  `id_marca` int(11) NOT NULL,
  `estado` bit(1) NOT NULL,
  `id_usuario_creacion` int(11) NOT NULL,
  `id_usuario_modificacion` int(11) NOT NULL,
  `fecha_creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fecha_modificacion` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estadoproducto`
--

CREATE TABLE `estadoproducto` (
  `id_estadoproducto` int(11) NOT NULL,
  `descripcion_estadoproducto` varchar(40) NOT NULL,
  `fechacreacion_estadoproducto` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fechamodificacion_estadoproducto` datetime DEFAULT NULL,
  `usuariocreacion_chequeoinventarioestadoproducto` varchar(20) NOT NULL,
  `usuariomodificacion_estadoproducto` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `estadoproducto`
--

INSERT INTO `estadoproducto` (`id_estadoproducto`, `descripcion_estadoproducto`, `fechacreacion_estadoproducto`, `fechamodificacion_estadoproducto`, `usuariocreacion_chequeoinventarioestadoproducto`, `usuariomodificacion_estadoproducto`) VALUES
(13, 'bueno', '2018-12-08 16:39:54', NULL, '', NULL),
(14, 'malo', '2018-12-08 16:39:55', NULL, '', NULL),
(15, 'bueno', '2018-12-08 16:40:04', NULL, '', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ficha`
--

CREATE TABLE `ficha` (
  `id_ficha` int(11) NOT NULL,
  `ficha_ficha` int(11) NOT NULL,
  `fecha_inicio_ficha` date NOT NULL,
  `fecha_fin_ficha` date NOT NULL,
  `jornada_ficha` varchar(100) NOT NULL,
  `id_ambiente` int(11) NOT NULL,
  `id_programa_formacion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ficha`
--

INSERT INTO `ficha` (`id_ficha`, `ficha_ficha`, `fecha_inicio_ficha`, `fecha_fin_ficha`, `jornada_ficha`, `id_ambiente`, `id_programa_formacion`) VALUES
(2, 15645, '2018-09-10', '2013-02-06', 'Tarde', 1, 2),
(5, 14444, '2018-12-03', '2018-12-30', 'MaÃ±ana', 1, 1),
(6, 1444, '2018-12-03', '2021-06-16', 'Tarde', 1, 1),
(7, 1525454, '2018-12-03', '2018-12-14', 'Noche', 1, 1),
(8, 2147483647, '2018-11-30', '2018-12-15', 'Tarde', 1, 1),
(11, 333333, '2018-11-30', '2018-12-15', 'Tarde', 1, 1),
(12, 22222, '2018-12-09', '2018-12-23', 'Tarde', 1, 1),
(13, 2222, '2018-12-09', '2018-12-23', 'Tarde', 1, 1),
(14, 222, '2018-12-09', '2018-12-23', 'Tarde', 1, 1),
(15, 22, '2018-12-09', '2018-12-23', 'Tarde', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gimnasio`
--

CREATE TABLE `gimnasio` (
  `id_gimnasio` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `id_ciudad` int(11) NOT NULL,
  `id_sede` int(11) NOT NULL,
  `id_usuario_creacion` int(11) NOT NULL,
  `id_usuario_modificacion` int(11) NOT NULL,
  `fecha_creacion` date NOT NULL,
  `fecha_modificacion` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `imc`
--

CREATE TABLE `imc` (
  `id_imc` int(11) NOT NULL,
  `descripcion_imc` varchar(30) NOT NULL,
  `fechacreacion_imc` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fechamodificacion_imc` datetime DEFAULT CURRENT_TIMESTAMP,
  `usuariocreacion_imc` varchar(20) NOT NULL,
  `usuariomodificacion_imc` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `imc`
--

INSERT INTO `imc` (`id_imc`, `descripcion_imc`, `fechacreacion_imc`, `fechamodificacion_imc`, `usuariocreacion_imc`, `usuariomodificacion_imc`) VALUES
(1, 'Bajo peso', '2018-12-09 17:12:07', '2018-12-09 17:12:07', '', NULL),
(2, 'Normal', '2018-12-09 17:12:07', '2018-12-09 17:12:07', '', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `instructor`
--

CREATE TABLE `instructor` (
  `id_instructor` int(11) NOT NULL,
  `id_persona` int(11) NOT NULL,
  `estado_instructor` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `instructor`
--

INSERT INTO `instructor` (`id_instructor`, `id_persona`, `estado_instructor`) VALUES
(2, 19, 'A'),
(3, 20, 'A'),
(5, 22, 'A'),
(8, 25, 'A');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `instructor_ficha`
--

CREATE TABLE `instructor_ficha` (
  `id_instructor_ficha` int(11) NOT NULL,
  `id_ficha` int(11) NOT NULL,
  `id_instructor` int(11) NOT NULL,
  `estado_instructor_ficha` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inventariogym`
--

CREATE TABLE `inventariogym` (
  `id_inventariogym` int(11) NOT NULL,
  `id_sede` int(11) NOT NULL,
  `elemento_inventariogym` varchar(50) NOT NULL,
  `cantidad_inventariogym` int(11) NOT NULL,
  `id_estadoproducto` int(11) NOT NULL,
  `imagen_inventariogym` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `jornada`
--

CREATE TABLE `jornada` (
  `id_jornada` int(11) NOT NULL,
  `descripcion` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marca`
--

CREATE TABLE `marca` (
  `id_marca` int(11) NOT NULL,
  `descripcion` varchar(50) NOT NULL,
  `id_usuario_creacion` int(11) NOT NULL,
  `id_usuario_modificacion` int(11) NOT NULL,
  `fecha_creacion` date NOT NULL,
  `fecha_modificacion` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `municipio`
--

CREATE TABLE `municipio` (
  `id_municipio` int(11) NOT NULL,
  `nombre_municipio` varchar(20) NOT NULL,
  `id_departamento` int(11) NOT NULL,
  `fechacreacion_municipio` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fechamodificacion_municipio` datetime DEFAULT CURRENT_TIMESTAMP,
  `usuariocreacion_municipio` varchar(20) NOT NULL,
  `usuariomodificacion_municipio` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `municipio`
--

INSERT INTO `municipio` (`id_municipio`, `nombre_municipio`, `id_departamento`, `fechacreacion_municipio`, `fechamodificacion_municipio`, `usuariocreacion_municipio`, `usuariomodificacion_municipio`) VALUES
(1, 'Barranquilla', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(2, 'Galapa', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(3, 'Malambo', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(4, 'Puerto Colombia', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(5, 'Soledad', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(6, 'Baranoa', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(7, 'Palmar de Valera', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(8, 'Polonuevo', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(9, 'Ponedera', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(10, 'Sabanagrande', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(11, 'Sabanalarga', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(12, 'Santo Tomás', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(13, 'Juan de Acosta', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(14, 'Piojó', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(15, 'Turbará', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(16, 'Usiacurí', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(17, 'Campo de la Cruz', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(18, 'Candelaria', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(19, 'Luruano', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(20, 'Manati', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(21, 'Repelòn', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(22, 'Santa Lucia', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(23, 'Suàn', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(24, 'Cartagena de Indias', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(25, 'Arjona', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(26, 'Arroyohondo', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(27, 'Calamar', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(28, 'Clemencia', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(29, 'Mahates', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(30, 'San Cristòbal', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(31, 'San Estanislao', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(32, 'Soplaviento', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(33, 'Santa Catalina', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(34, 'Santa Rosa', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(35, 'Turbaco', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(36, 'Turbanà', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(37, 'Villanueva', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(38, 'Achì', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(39, 'Maganguè', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(40, 'Montecristo', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(41, 'Pinillos', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(42, 'San Jacinto del Cauc', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(43, 'Tiquisio', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(44, 'Altos del Rosario', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(45, 'Barranco de Loba', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(46, 'El Peñon', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(47, 'San Martin de Loba', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(48, 'Regidor', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(49, 'Rioviejo', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(50, 'Norosì', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(51, 'Arenal', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(52, 'Cantagallo', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(53, 'Morales', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(54, 'San Pablo', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(55, 'Santa Rosa del Sur', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(56, 'Simitì', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(57, 'El Carmen de Bolivar', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(58, 'El Guamo', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(59, 'Marìa la Baja', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(60, 'San Jacinto', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(61, 'Cordoba', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(62, 'Zambrano', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(63, 'Cicuco', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(64, 'Hatio de Loba', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(65, 'Hatio de Loba', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(66, 'Margarita', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(67, 'San Fernando', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(68, 'Santa Cruz de Mompox', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(69, 'Talaigua Nuevo', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(70, 'Valledupar', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(71, 'Aguachica', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(72, 'Agustìn Codazzi', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(73, 'Bosconia', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(74, 'Chimichagua', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(75, 'El Copey', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(76, 'San Alberto', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(77, 'Curumanì', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(78, 'El Paso', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(79, 'La Paz Robles', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(80, 'Pueblobello', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(81, 'La Jagua de Ibirico', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(82, 'Chiriguanà', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(83, 'Astrea', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(84, 'San Martin', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(85, 'Pelaya', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(86, 'Pailitas', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(87, 'Gamarra', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(88, 'Manaure', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(89, 'Rìo de Oro', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(90, 'Tamalameque', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(91, 'Becerril', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(92, 'San Diego', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(93, 'La Gloria', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(94, 'Gonzàlez', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(95, 'Montería', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(96, 'Ayapel', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(97, 'Buenavista', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(98, 'Canalete', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(99, 'Cereté', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(100, 'Chimá', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(101, 'Chinú', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(102, 'Ciénaga de Oro', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(103, 'Cotorra', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(104, 'La Apartada', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(105, 'Los Córdobas', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(106, 'Momil', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(107, 'Montelíbano', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(108, 'Moñitos', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(109, 'Planeta Rica', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(110, 'Pueblo Nuevo', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(111, 'Puerto Escondido', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(112, 'Puerto Libertador', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(113, 'Purísima', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(114, 'Sahagún', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(115, 'San Andrés de Sotave', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(116, 'San Antero', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(117, 'San Bernardo del Vie', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(118, 'San Carlos', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(119, 'San José de Uré', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(120, 'San Pelayo', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(121, 'Santa Cruz de Lorica', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(122, 'Tierralta', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(123, 'Tuchín', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(124, 'Valencia', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(125, 'Riohacha', 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(126, 'Albania', 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(127, 'Barrancas', 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(128, 'Dibulla', 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(129, 'Distracción', 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(130, 'El Molino', 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(131, 'Fonseca', 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(132, 'Hatonuevo', 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(133, 'La Jaguar del Pilar', 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(134, 'Maicao', 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(135, 'Manaure', 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(136, 'San Juan del César', 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(137, 'Uribia', 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(138, 'Urumita', 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(139, 'Villanueva', 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(140, 'Santa Marta', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(141, 'Algarrobo', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(142, 'Aracataca', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(143, 'Ariguaní', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(144, 'Cerro de San Antonio', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(145, 'Chibolo', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(146, 'Ciénaga', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(147, 'Concordia', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(148, 'El Banco', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(149, 'El Piñon', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(150, 'El Retén', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(151, 'Fundaciòn', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(152, 'Guamal', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(153, 'Nueva Granada', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(154, 'Pedraza', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(155, 'Pijino del Carmen', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(156, 'Pivijai', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(157, 'Plato', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(158, 'Pueblo viejo', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(159, 'Remolino', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(160, 'Sabanas de San Ángel', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(161, 'Salamina', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(162, 'San Sebastián de Bue', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(163, 'Santa Ana', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(164, 'Santa Bárbara de Pin', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(165, 'San Zenón', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(166, 'Sitionuevo', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(167, 'Tenerife', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(168, 'Zapayán', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(169, 'Zona Bananera', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(170, 'San Andres', 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(171, 'Providencia y Santa ', 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(172, 'Sincelejo', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(173, 'Chalán', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(174, 'Colosó', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(175, 'Morroa', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(176, 'Ovejas', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(177, 'Guaranda', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(178, 'Majagual', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(179, 'Sucre', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(180, 'Coveñas', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(181, 'Palmito', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(182, 'San Onofre', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(183, 'Santiago de Tolú', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(184, 'Tolú viejo', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(185, 'Buenavista', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(186, 'Corozal', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(187, 'El Roble', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(189, 'Galeras', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(190, 'Los Palmitos', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(191, 'Sampués', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(192, 'San Juan de Betulia', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(193, 'San Pedro', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(194, 'Sincé', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(195, 'Caimito', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(196, 'La Unión', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(197, 'San Benito', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(198, 'San Marcos', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(199, 'Cáceres', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(200, 'Caucasia', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(201, 'El Bagre', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(202, 'Nechí', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(203, 'Tarazá', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(204, 'Zaragoza', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(205, 'Caracolí', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(206, 'Maceo', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(207, 'Puerto Berrío', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(208, 'Puerto Nare', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(209, 'Puerto Triunfo', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(210, 'Yondó', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(211, 'Amalfi', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(212, 'Anorí', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(213, 'Cisneros', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(214, 'Remedios', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(215, 'San Roque', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(216, 'Santo Domingo', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(217, 'Segovia', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(218, 'Vegachí', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(219, 'Yalí', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(220, 'Yolombó', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(221, 'Angostura', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(222, 'Belmira', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(223, 'Briceño', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(224, 'Campamento', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(225, 'Carolina del Príncip', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(226, 'Donmatías', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(227, 'Entrerríos', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(228, 'Gómez Plata', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(229, 'Guadalupe', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(230, 'Ituango', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(231, 'San Andrés de Cuerqu', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(232, 'San José de la Monta', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(233, 'San Pedro de los Mil', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(234, 'Santa Rosa de Osos', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(235, 'Toledo', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(236, 'Valdivia', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(237, 'Yarumal', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(238, 'Abriaquí', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(239, 'Antioquia', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(240, 'Anzá', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(241, 'Armenia', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(242, 'Buriticá', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(243, 'Caicedo', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(244, 'Cañasgordas', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(245, 'Dabeiba', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(246, 'Ebéjico', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(247, 'Frontino', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(248, 'Giraldo', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(249, 'Heliconia', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(250, 'Liborina', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(251, 'Olaya', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(252, 'Peque', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(253, 'Sabanalarga', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(254, 'San Jerónimo', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(255, 'Sopetrán', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(256, 'Uramita', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(257, 'Abejorral', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(258, 'Alejandría', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(259, 'Argelia', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(260, 'Carmen de Viboral', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(261, 'Cocorná', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(262, 'Concepción', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(263, 'El Peñol', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(264, 'El Retiro', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(265, 'El Santuario', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(266, 'Granada', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(267, 'Guarne', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(268, 'Guatapé', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(269, 'La Ceja', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(270, 'La Unión', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(271, 'Marinilla', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(272, 'Nariño', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(273, 'Rionegro', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(274, 'San Carlos', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(275, 'San Francisco', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(276, 'San Luis', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(277, 'San Rafael', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(278, 'San Vicente', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(279, 'Sonsón', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(280, 'Amagá', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(281, 'Andes', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(282, 'Angelópolis', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(283, 'Betania', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(284, 'Betulia', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(285, 'Caramanta', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(286, 'Ciudad Bolívar', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(287, 'Concordia', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(288, 'Fredonia', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(289, 'Hispania', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(290, 'Jardín', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(291, 'Jericó', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(292, 'La Pintada', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(293, 'Montebello', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(294, 'Pueblorrico', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(295, 'Salgar', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(296, 'Santa Bárbara', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(297, 'Támesis', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(298, 'Tarso', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(299, 'Titiribí', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(300, 'Urrao', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(301, 'Valparaíso', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(302, 'Venecia', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(303, 'Apartadó', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(304, 'Arboletes', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(305, 'Carepa', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(306, 'Chigorodó', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(307, 'Murindó', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(308, 'Mutatá', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(309, 'Necoclí', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(310, 'San Juan de Urabá', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(311, 'San Pedro de Urabá', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(312, 'Turbo', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(313, 'Vigía del Fuerte', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(314, 'Barbosa', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(315, 'Bello', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(316, 'Caldas', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(317, 'Copacabana', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(318, 'Envigado', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(319, 'Girardota', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(320, 'Itagüí', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(321, 'La Estrella', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(322, 'Medellín', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(323, 'Sabaneta', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(324, 'Tunja', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(325, 'Chíquiza', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(326, 'Chivatá', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(327, 'Cómbita', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(328, 'Cucaita', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(329, 'Motavita', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(330, 'Oicatá', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(331, 'Samacá', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(332, 'Siachoque', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(333, 'Sora', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(334, 'Soracá', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(335, 'Sotaquirá', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(336, 'Toca', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(337, 'Tuta', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(338, 'Ventaquemada', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(339, 'Chiscas', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(340, 'El cocuy', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(341, 'El espino', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(342, 'Guacamayas', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(343, 'Güicán', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(344, 'Panqueba', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(345, 'Labranzagrande', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(346, 'Pajarito', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(347, 'Paya', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(348, 'Pisba', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(349, 'Berbeo', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(350, 'Campo hermoso', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(351, 'Miraflores', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(352, 'Páez', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(353, 'San Eduardo', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(354, 'Zetaquira', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(355, 'Boyacá', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(356, 'Ciénega', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(357, 'Jenesano', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(358, 'Nuevo Colón', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(359, 'Ramiquirí', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(360, 'Rondón', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(361, 'Tibaná', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(362, 'Turmequé', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(363, 'Úmbita', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(364, 'Viracachá', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(365, 'Chinavita', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(366, 'Garagoa', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(367, 'Macanal', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(368, 'Pachavita', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(369, 'San Luis de Gaceno', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(370, 'Santa María', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(371, 'Boavita', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(372, 'Covarachía', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(373, 'La Uvita', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(374, 'San Mateo', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(375, 'Sativanorte', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(376, 'Sativasur', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(377, 'Soatá', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(378, 'Susacón', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(379, 'Tipacoque', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(380, 'Briceño', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(381, 'Buenavista', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(382, 'Caldas', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(383, 'Chiquinquirá', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(384, 'Coper', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(385, 'La victoria', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(386, 'Maripí', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(387, 'Muzo', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(388, 'Otanche', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(389, 'Pauna', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(390, 'Quípama', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(391, 'Saboyá', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(392, 'San Miguel de Sema', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(393, 'San Pablo de Borbur', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(394, 'Tununguá', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(395, 'Almeida', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(396, 'Chivor', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(397, 'Guateque', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(398, 'Guayatá La capilla', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(399, 'Somondoco', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(400, 'Sutatenza', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(401, 'Tenza', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(402, 'Arcabuco', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(403, 'Chitaraque', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(404, 'Gachantivá', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(405, 'Moniquirá', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(406, 'Ráquira', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(407, 'Sáchica', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(408, 'San Jose de Pare', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(409, 'Santa Sofía', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(410, 'Santana', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(411, 'Sutamarchán', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(412, 'Tijancá', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(413, 'Togüí', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(414, 'Villa de Leyva', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(415, 'Aquitania', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(416, 'Cuítiva', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(417, 'Firavitoba', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(418, 'Gámeza', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(419, 'Iza', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(420, 'Mongua', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(421, 'Monguí', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(422, 'Nobsa', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(423, 'Pesca', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(424, 'Sogamoso', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(425, 'Tibasosa', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(426, 'Tópaga', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(427, 'Tota', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(428, 'Belén', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(429, 'Busbanzá', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(430, 'Cerinza', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(431, 'Corrales', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(432, 'Duitama', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(433, 'Floresta', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(434, 'Paipa', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(435, 'Santa Rosa de Viterb', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(436, 'Tutazá', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(437, 'Betéitiva', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(438, 'Chita', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(439, 'Jericó', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(440, 'Paz de Río', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(441, 'Socha', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(442, 'Socotá', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(443, 'Tasco', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(444, 'Cubará', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(445, 'Puerto Boyacá', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(446, 'Manizales', 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(447, 'Chinchiná', 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(448, 'Neira', 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(449, 'Palestina', 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(450, 'Villamaría', 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(451, 'Filadelfia', 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(452, 'La Merced', 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(453, 'Marmato', 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(454, 'Riosucio', 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(455, 'Supía', 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(456, 'Manzanares', 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(457, 'Marquetalia', 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(458, 'Marulanda', 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(459, 'Pensilvania', 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(460, 'Anserma', 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(461, 'Belalcazár', 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(462, 'Risaralda', 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(463, 'San José', 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(464, 'Viterbo', 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(465, 'La Dorada', 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(466, 'Norcasia', 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(467, 'Samaná', 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(468, 'Victoria', 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(469, 'Aguadas', 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(470, 'Aranzazu', 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(471, 'Pácora', 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(472, 'Salamina', 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(473, 'Chocontá', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(474, 'Machetá', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(475, 'Manta', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(476, 'Sesquilé', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(477, 'Suesca', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(478, 'Tibirita', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(479, 'Villapinzón', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(480, 'Agua de Dios', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(481, 'Girardot', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(482, ' Guataquí', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(483, 'Jerusalén', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(484, 'Nariño', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(485, 'Nilo', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(486, 'Ricaurte', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(487, 'Tocaima', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(488, 'Caparrapí', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(489, 'Guaduas', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(490, 'Puerto Salgar', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(491, 'Albán', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(492, 'La Peña', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(493, 'La Vega', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(494, 'Nimaima', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(495, 'Nocaima', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(496, 'Quebradanegra', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(497, 'San Francisco', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(498, 'Sasaima', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(499, 'Supatá', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(500, 'Útica', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(501, 'Vergara', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(502, 'Villeta', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(503, 'Gachalá', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(504, 'Gachetá', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(505, 'Gama', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(506, 'Guasca', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(507, 'Guatavita', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(508, 'Junín', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(509, 'La Calera', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(510, 'Ubalá', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(511, 'Beltrán', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(512, 'Bituima', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(513, 'Chaguaní', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(514, 'Guayabal de Síquima', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(515, 'Pulí', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(516, 'San Juan de Rioseco', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(517, 'Vianí', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(518, 'Medina', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(519, 'Paratebueno', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(520, 'Cáqueza', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(521, 'Chipaque', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(522, 'Choachí', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(523, 'Fómeque', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(524, 'Fosca', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(525, 'Guayabetal', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(526, 'Gutiérrez', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(527, 'Quetame', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(528, 'Ubaque', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(529, 'Une', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(530, 'El peñón', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(531, 'La palma', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(532, 'Pacho', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(533, 'Paime', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(534, 'San Cayetano', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(535, 'Toaipí', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(536, 'Villagómez', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(537, 'Yacopí', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(538, 'Cajicá', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(539, 'Chía', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(540, 'Cota', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(541, 'Cogua', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(542, 'Gachancipá', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(543, 'Nemocón', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(544, 'Sopó', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(545, 'Tabio', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(546, 'Tenjo', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(547, 'Tocancipá', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(548, 'Zipaquirá', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(549, 'Bojacá', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(550, 'El Rosal', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(551, 'Facatativá', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(552, 'Funza', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(553, 'Madrid', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(554, 'Mosquera', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(555, 'Subachoque', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(556, 'Zipacón', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(557, 'Sibaté', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(558, 'Soacha', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(559, 'Arbeláez', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(560, 'Cabrera', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(561, 'Fusagasugá', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(562, 'Granada', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(563, 'Pandi', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(564, 'Pasca', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(565, 'San Bernardo', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(566, 'Silvania', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(567, 'Tibacuy', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(568, 'Anapoima', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(569, 'Analaima', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(570, 'Cachipay', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(571, 'El colegio', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(572, 'La mesa', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(573, 'Quipile', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(574, 'San Antonio del Tequ', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(575, 'Tena', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(576, 'Viotá', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(577, 'Carmen de Carupa', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(578, 'Cucunubá', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(579, 'Fúneque', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(580, 'Guachetá', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(581, 'Lenguazaque', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(582, 'Simijaca', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(583, 'Susa', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(584, 'Sutatausa', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(585, 'Tausa', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(586, 'Ubaté', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(587, 'Neiva', 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(588, 'Aipe', 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(589, 'Algeciras', 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(590, 'Baraya', 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(591, 'Campoalegre', 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(592, 'Colombia', 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(593, 'Hobo', 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(594, 'Íquira', 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(595, 'Palermo', 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(596, 'Rivera', 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(597, 'Santa María', 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(598, 'Tello', 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(599, 'Teruel', 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(600, 'Villavieja', 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(601, 'Yaguará', 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(602, 'Agrado', 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(603, 'Altamira', 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(604, 'Garzón', 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(605, 'Gigante', 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(606, 'Guadalupe', 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(607, 'Pital', 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(608, 'Suaza', 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(609, 'Tarqui', 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(610, 'La Argentina', 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(611, 'La Plata', 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(612, 'Nátaga', 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(613, 'Paicol', 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(614, 'Tesalia', 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(615, 'Acevedo', 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(616, 'Elías', 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(617, 'Isnos', 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(618, 'Oporapa', 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(619, 'Palestina', 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(620, 'Pitalito', 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(621, 'Saladoblanco', 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(622, 'San Agustín', 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(623, 'Timaná', 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(624, 'San José de Cúcuta', 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(625, 'Abrego', 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(626, 'Arboledas', 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(627, 'Bochalema', 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(628, 'Bucarasica', 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(629, 'Cáchira', 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(630, 'Cácota', 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(631, 'Chinácota', 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(632, 'Chitagá', 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(633, 'Convención', 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(634, 'Cucutilla', 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(635, 'Durania', 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(636, 'El Carmen', 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(637, 'El Tarra', 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(638, 'El Zulia', 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(639, 'Gramalote', 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(640, 'Hacarí', 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(641, 'Herrán', 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(642, 'La Esperanza', 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(643, 'La Playa de Belén', 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(644, 'Labateca', 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(645, 'Los Patios', 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(646, 'Lourdes', 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(647, 'Mutiscua', 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(648, 'Ocaña', 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(649, 'Pamplona', 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(650, 'Pamplonita', 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(651, 'Puerto Santander', 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(652, 'Ragonvalia', 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(653, 'Salazar de las Palma', 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(654, 'San Calixto', 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(655, 'San Cayetano', 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(656, 'Santiago', 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(657, 'Sardinata', 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(658, 'Silos', 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(659, 'Teorama', 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(660, 'Tibú', 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(661, 'Toledo', 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(662, 'Villa Caro', 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(663, 'Villa del Rosario', 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(664, 'Armenia', 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '');
INSERT INTO `municipio` (`id_municipio`, `nombre_municipio`, `id_departamento`, `fechacreacion_municipio`, `fechamodificacion_municipio`, `usuariocreacion_municipio`, `usuariomodificacion_municipio`) VALUES
(665, 'Buenavista', 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(666, 'Calarcá', 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(667, 'Circasia', 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(668, 'Córdoba', 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(669, 'Finlandia', 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(670, 'Génova', 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(671, 'La Tebaida', 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(672, 'Montenegro', 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(673, 'Pijao', 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(674, 'Quimbaya', 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(675, 'Salento', 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(676, 'Pereira', 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(677, 'Apía', 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(678, 'Balboa', 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(679, 'Belén de Umbría', 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(680, 'Dosquebradas', 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(681, 'Guática', 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(682, 'La Celia', 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(683, 'La Virginia', 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(684, 'Marsella', 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(685, 'Mistrató', 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(686, 'Pueblo Rico', 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(687, 'Quinchía', 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(688, 'Santa Rosa de Cabal', 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(689, 'Santuario', 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(690, 'Bucaramanga', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(691, 'Floridablanca', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(692, 'Girón', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(693, 'Lebrija', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(694, 'Los Santos', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(695, 'Piedecuesta', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(696, 'Rionegro', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(697, 'Santa Bárbara', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(698, 'Barrancabermeja', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(699, 'Betulia', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(700, 'El Carmen de Chucurí', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(701, 'Puerto Wilches', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(702, 'Sabana de torres', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(703, 'San Vicente', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(704, 'Zapatoca', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(705, 'California', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(706, 'Charta', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(707, 'El Playón', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(708, 'Matanza', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(709, 'Suratá', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(710, 'Tona', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(711, 'Vetas', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(712, 'Capitanejo', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(713, 'Carcasí', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(714, 'Cerrito', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(715, 'Concepción', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(716, 'Enciso', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(717, 'Guaca', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(718, 'Macaravita', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(719, 'Málaga', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(720, 'Molagavita', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(721, 'San Andrés', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(722, 'San José de Miranda', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(723, 'San Miguel', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(724, 'Aratoca', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(725, 'Barichara', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(726, 'Cabrera', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(727, 'Cepitá', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(728, 'Charalá', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(729, 'Coromoro', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(730, 'Curití', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(731, 'Encino', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(732, 'Jordán', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(733, 'Mogotes', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(734, 'Ocamonte', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(735, 'Onzaga', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(736, 'Parámo', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(737, 'Pinchote', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(738, 'San Gil', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(739, 'San Joaquín', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(740, 'Valle de San José', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(741, 'Villanueva', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(742, 'Aguada', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(743, 'Albania', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(744, 'Barbosa', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(745, 'Bolívar', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(746, 'Chipatá', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(747, 'El peñón', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(748, 'Florián', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(749, 'Guavatá', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(750, 'Guepsa', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(751, 'Jesus María', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(752, 'La Belleza', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(753, 'La Paz', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(754, 'Puente Nacional', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(755, 'San Benito', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(756, 'Sucre', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(757, 'Veléz', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(758, 'Suaita', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(759, 'Socorro', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(760, 'Simacota', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(761, 'Palmas del socorro', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(762, 'Palmar', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(763, 'Oiba', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(764, 'Hato', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(765, 'Guapotá', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(766, 'Guadalupe', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(767, 'Gámbita', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(768, 'Galán', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(769, 'El Guacamayo', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(770, 'Contratación', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(771, 'Confines', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(772, 'Chima', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(773, 'Cimitarra', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(774, 'Landázuri', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(775, 'Puerto Parra', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(776, 'Santa Helena del Opó', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(777, 'Ibagué', 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(778, 'Alvarado', 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(779, 'Anzoátegui', 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(780, 'Cajamarca', 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(781, 'Coello', 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(782, 'Espinal', 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(783, 'Flandes', 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(784, 'Piedras', 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(785, 'Rovira', 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(786, 'San Luis', 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(787, 'Valle de San Juan', 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(788, 'Casablanca', 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(789, 'Herveo', 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(790, 'Lérida', 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(791, 'Líbano', 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(792, 'Murillo', 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(793, 'Santa Isabel', 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(794, 'Venadillo', 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(795, 'Villahermosa', 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(796, 'Ambalema', 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(797, 'Armero', 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(798, 'Falán', 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(799, 'Fresno', 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(800, 'Honda', 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(801, 'Mariquita', 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(802, 'Palocabildo', 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(803, 'Carmen de Apicalá', 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(804, 'Cunday', 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(805, 'Icononzo', 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(806, 'Melgar', 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(807, 'Villarica', 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(808, 'Ataco', 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(809, 'Chaparral', 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(810, 'Coyaima', 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(811, 'Natagaima', 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(812, 'Ortega', 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(813, 'Planadas', 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(814, 'Rioblanco', 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(815, 'Roncesvalle', 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(816, 'San Antonio', 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(817, 'Alpujarra', 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(818, 'Dolores', 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(819, 'Guamo', 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(820, 'Prado', 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(821, 'Purificación', 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(822, 'Saldaña', 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(823, 'Suárez', 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(824, 'Popayán', 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(825, 'Cajibío', 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(826, 'El tambo', 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(827, 'La Sierra', 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(828, 'Morales', 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(829, 'Piendamó', 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(830, 'Rosas', 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(831, 'Soatá', 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(832, 'Timbío ', 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(833, 'Buenos aires', 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(834, 'Caloto', 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(835, 'Corintio', 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(836, 'Guachené', 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(837, 'Miranda', 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(838, 'Padilla', 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(839, 'Puerto Tejada', 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(840, 'Santander de Quilich', 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(841, 'Suárez', 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(842, 'Villa rica', 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(843, 'Almaguer', 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(844, 'Argelia', 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(845, 'Balboa', 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(846, 'Bolívar', 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(847, 'Florencia', 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(848, 'La vega', 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(849, 'Mercaderes', 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(850, 'Patía', 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(851, 'Piamonte', 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(852, 'San Sebastián', 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(853, 'Santa Rosa', 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(854, 'Sucre', 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(855, 'Guapí', 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(856, 'López de micay', 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(857, 'Timbiquí', 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(858, 'Caldono', 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(859, 'Inzá', 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(860, 'Jambaló', 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(861, 'Páez', 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(862, 'Puracé', 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(863, 'Silvia', 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(864, 'Toribío', 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(865, 'Torotó', 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(866, 'Quibdó', 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(867, 'Acandí', 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(868, 'Alto Baudó', 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(869, 'Atrato', 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(870, 'Bagadó', 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(871, 'Bahía Solano', 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(872, 'Bajo Baudó', 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(873, 'Bojayá', 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(874, 'Cértegui', 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(875, 'Condoto', 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(876, 'El Cantón de San Pab', 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(877, 'El Carmen de Atrato', 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(878, 'El Carmen del Darién', 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(879, 'Litoral del San Juan', 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(880, 'Istmina', 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(881, 'Juradó', 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(882, 'Lloró', 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(883, 'Medio Atrato', 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(884, 'Medio Baudó', 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(885, 'Medio San Juan', 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(886, 'Nóvita', 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(887, 'Nuquí', 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(888, 'Río Iró', 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(889, 'Río Quito', 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(890, 'Riosucio', 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(891, 'San José del Palmar', 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(892, 'Sipí', 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(893, 'Tadó', 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(894, 'Unguía', 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(895, 'Unión Panamericana', 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(896, 'Pasto', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(897, 'Buesaco', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(898, 'Chachagüí', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(899, 'Consacá', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(900, 'El Peñol', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(901, 'El Tambo', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(902, 'La Florida', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(903, 'Nariño', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(904, 'Sandoná', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(905, 'Tangua', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(906, 'Yacuanquer', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(907, 'Barbacoas', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(908, 'El Charco', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(909, 'Francisco Pizarro', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(910, 'La Tola', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(911, 'Magüí', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(912, 'Mosquera', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(913, 'Olaya Herrera', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(914, 'Roberto Payán', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(915, 'Santa Bárbara', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(916, 'Tumaco', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(917, 'Aldana', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(918, 'Contadero', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(919, 'Córdoba', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(920, 'Cuaspud', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(921, 'Cumbal', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(922, 'Funes', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(923, 'Guachucal', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(924, 'Gualmatán', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(925, 'Iles', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(926, 'Ipiales', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(927, 'Potosí', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(928, 'Puerres', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(929, 'Pupiales', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(930, 'Albán', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(931, 'Arboleda', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(932, 'Belén', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(933, 'Colón', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(934, 'El Rosario', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(935, 'El Tablón de Gómez', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(936, 'La Cruz', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(937, 'La Unión', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(938, 'Leiva', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(939, 'Policarpa', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(940, 'San Bernardo', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(941, 'San Lorenzo', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(942, 'San Pablo', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(943, 'San Pedro de Cartago', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(944, 'Taminango', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(945, 'Ancuyá', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(946, 'Cumbitara', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(947, 'Guaitarilla', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(948, 'Imués', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(949, 'La Llanada', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(950, 'Linares', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(951, 'Los Andes', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(952, 'Mallama', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(953, 'Ospina', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(954, 'Providencia', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(955, 'Ricaurte', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(956, 'Samaniego', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(957, 'Santacruz', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(958, 'Sapuyes', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(959, 'Túquerres', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(960, 'Cali', 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(961, 'Candelaria', 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(962, 'Dagua', 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(963, 'Florida', 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(964, 'Jamundí', 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(965, 'La Cumbre', 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(966, 'Palmira', 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(967, 'Pradera', 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(968, 'Vijes', 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(969, 'Yumbo', 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(970, 'Andalucía', 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(971, 'Buga', 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(972, 'Bugalagrande', 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(973, 'Calima- El Darién', 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(974, 'El Cerrito', 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(975, 'Ginebra', 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(976, 'Guacarí', 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(977, 'Restrepo', 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(978, 'Riofrío', 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(979, 'San Pedro', 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(980, 'Trujillo', 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(981, 'Tuluá', 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(982, 'Yotoco', 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(983, 'Buenaventura', 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(984, 'Caicedonia', 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(985, 'Sevilla', 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(986, 'Cartago', 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(987, 'El águila', 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(988, 'El Cairo', 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(989, 'El Dovio', 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(990, 'La Unión', 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(991, 'La Victoria', 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(992, 'Obando', 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(993, 'Restrepo', 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(994, 'Rodalnillo', 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(995, 'Toro', 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(996, 'Ulloa', 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(997, 'Versalles', 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(998, 'Zarzal', 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(999, 'Alcalá', 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1000, 'Arauca', 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1001, 'Arauquita', 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1002, 'Cravo Norte', 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1003, 'Fortul', 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1004, 'Puerto Rondón', 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1005, 'Saravena', 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1006, 'Tame', 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1007, 'Yopal', 24, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1008, 'Aguazul', 24, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1009, 'Chámeza', 24, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1010, 'Hato Corozal', 24, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1011, 'La Salina', 24, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1012, 'Maní', 24, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1013, 'Monterrey', 24, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1014, 'Nunchía', 24, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1015, 'Orocué', 24, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1016, 'Paz de Ariporo', 24, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1017, 'Pore', 24, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1018, 'Recetor', 24, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1019, 'Sabanalarga', 24, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1020, 'Sácama', 24, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1021, 'San Luis de Palenque', 24, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1022, 'Támara', 24, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1023, 'Tauramena', 24, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1024, 'Trinidad', 24, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1025, 'Villanueva', 24, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1026, 'Villavicencio', 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1027, 'Acacías', 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1028, 'Barranca de Upía', 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1029, 'Castilla La Nueva', 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1030, 'Cubarral', 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1031, 'Cumaral', 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1032, 'El Calvario', 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1033, 'Guamal', 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1034, 'Restrepo', 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1035, 'San Carlos de Guaroa', 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1036, 'San Juanito', 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1037, 'San Martín', 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1038, 'Cabuyaro', 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1039, 'Puerto Gaitán', 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1040, 'Puerto López', 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1041, 'El Castillo', 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1042, 'El Dorado', 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1043, 'Fuente de Oro', 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1044, 'Granada', 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1045, 'La Macarena', 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1046, 'La Uribe', 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1047, 'Lejanías', 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1048, 'Mapiripan', 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1049, 'Mesetas', 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1050, 'Puerto Concordia', 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1051, 'Puerto Lleras', 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1052, 'Puerto Rico', 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1053, 'San Juan de Arama', 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1054, 'Vista hermosa', 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1055, 'Puerto Carreño', 26, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1056, 'La Primavera', 26, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1057, 'Santa Rosalía', 26, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1058, 'Cumaribo', 26, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1059, 'Leticia', 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1060, 'Puerto Nariño', 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1061, 'Florencia', 28, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1062, 'Albania', 28, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1063, 'Belén de los andaqui', 28, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1064, 'Cartagena del Chairá', 28, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1065, 'Curillo', 28, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1066, 'El Doncello', 28, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1067, 'El Paujil', 28, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1068, 'La montañita', 28, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1069, 'Morelia', 28, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1070, 'Puerto Milán', 28, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1071, 'Puerto Rico', 28, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1072, 'San José del Fragua', 28, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1073, 'San Vicente del Cagu', 28, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1074, 'Solano', 28, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1075, 'Solitaria', 28, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1076, 'Valparaíso', 28, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1077, 'Inirida', 29, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1078, 'San José del Guaviar', 30, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1079, 'Calamar', 30, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1080, 'El Retorno', 30, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1081, 'Miraflores', 30, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1082, 'Mocoa', 31, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1083, 'Colón', 31, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1084, 'Orito', 31, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1085, 'Puerto Asís', 31, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1086, 'Puerto Caicedo', 31, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1087, 'Puerto Guzmán', 31, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1088, 'Puerto Leguízamo', 31, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1089, 'San Francisco', 31, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1090, 'San Miguel', 31, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1091, 'Santiago', 31, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1092, 'Sibundoy', 31, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1093, 'Valle del Guamuez', 31, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1094, 'Villagarzón', 31, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1095, 'Mitu', 32, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1096, 'Carurú', 32, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(1097, 'Taraira', 32, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nivelprograma`
--

CREATE TABLE `nivelprograma` (
  `id_nivelprograma` int(11) NOT NULL,
  `descripcion_nivelprograma` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `nivelprograma`
--

INSERT INTO `nivelprograma` (`id_nivelprograma`, `descripcion_nivelprograma`) VALUES
(1, 'asd');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nivel_programa`
--

CREATE TABLE `nivel_programa` (
  `id_nivel_programa` int(11) NOT NULL,
  `descripcion_nivel` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `id_usuario_creacion` int(11) NOT NULL,
  `id_usuario_modificacion` int(11) NOT NULL,
  `fecha_creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fecha_modificacion` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticia`
--

CREATE TABLE `noticia` (
  `id_noticia` int(11) NOT NULL,
  `lugar_noticia` varchar(30) NOT NULL,
  `fecha_noticia` date NOT NULL,
  `descripcion_noticia` varchar(2000) NOT NULL,
  `titulo_noticia` varchar(100) NOT NULL,
  `foto_noticia` varchar(30) NOT NULL,
  `id_sede` int(11) NOT NULL,
  `fechacreacion_noticia` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fechamodificacion_noticia` datetime DEFAULT CURRENT_TIMESTAMP,
  `usuariocreacion_noticia` varchar(20) NOT NULL,
  `usuariomodificacion_noticia` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Volcado de datos para la tabla `noticia`
--

INSERT INTO `noticia` (`id_noticia`, `lugar_noticia`, `fecha_noticia`, `descripcion_noticia`, `titulo_noticia`, `foto_noticia`, `id_sede`, `fechacreacion_noticia`, `fechamodificacion_noticia`, `usuariocreacion_noticia`, `usuariomodificacion_noticia`) VALUES
(1, 'Neiva', '2018-12-01', 'Los aspirantes a ocupar los cargos en los programas SENNOVA, AgroSENA y Bilinguismo, deben ser empleados de carrera administrativa, de la entidad.', 'SENA abre convocatoria para proveer 95 cargos temporales', 'convocatoria.jpg', 1, '2018-12-08 15:24:53', '2018-12-05 19:27:16', '', NULL),
(3, 'Neiva', '2018-12-03', 'Bajo este lema el Centro de Formacion en Actividad Fisica y Cultura del SENA, reunio a unas 1.700 personas, en la sexta edicion de la carrera 5k y maraton aerobica, que se realizo en el Parque Simon Bolivar.', 'Actividad fisica por la convivencia, contra la violencia', 'carrera.jpg', 2, '2018-12-08 15:37:47', '2018-12-08 15:37:47', '', NULL),
(4, 'Neiva', '2018-12-04', 'En el Caqueta se producen anualmente unas 40 toneladas de Pirarucu en 14 granjas piscicolas destinadas para tal fin.', 'SENA impulsa la produccion y comercializacion del Pirarucu en la Amazonia.', 'Pirarucu.jpg', 1, '2018-12-08 15:37:47', '2018-12-08 15:37:47', '', NULL),
(5, 'Neiva', '2018-12-05', 'Los estudiantes, que pertenecen a 21 instituciones educativas de Medellin y su area metropolitana, desarrollan habilidades en ciencia, tecnologia e innovacion durante su paso por la Tecnoacademia del SENA.', 'El SENA en Tecnociencia reunio a mas de 1.200 estudiantes', 'Tecnociancia.jpg', 2, '2018-12-08 15:42:20', '2018-12-08 15:42:20', '', NULL),
(6, 'Neiva', '2018-12-07', 'La Entidad dijo presente en la octava version del Premio Nacional Inventor Colombiano donde logro el reconocimiento en tres de las cuatro categorias de este premio.', 'El SENA protagonista en el Premio Nacional 2018 Inventor Colombiano', 'ganadorInventor.jpg', 1, '2018-12-08 15:42:20', '2018-12-08 15:42:20', '', NULL),
(7, 'Neiva', '2018-12-10', 'En alianza con la Asociacion Nacional de Recicladores (ANR), el SENA desarrollo el proceso de evaluacion y certificacion de competencias laborales a 98 recicladores en Bogota.', 'El SENA Certifica a recicladores en Bogota', 'Recicladores.jpg', 2, '2018-12-08 15:46:35', '2018-12-08 15:46:35', '', NULL),
(8, 'Neiva', '2018-12-11', 'Con la puesta de la primera piedra por parte del SENA, la gobernacion del Atlantico y la alcaldia de Soledad, se dio apertura a la edificacion de la sede de Operaciones Comerciales que contara con 1.075 metros cuadrados de area construida.', 'Soledad (Atlantico) tendra nueva sede del SENA', 'soledadAtlantico.jpg', 2, '2018-12-08 15:47:02', '2018-12-08 15:46:35', '', NULL),
(9, 'Neiva', '2018-12-12', 'Este gran acuerdo reune a representantes del ecosistema emprendedor colombiano, quienes abordaran los principales retos frente a la creacion de empresas y proyectos productivos en el pais.', 'SENA firma el Pacto Nacional por el Emprendimiento', 'pactoEmprendimiento.jpg', 1, '2018-12-08 15:51:07', '2018-12-08 15:50:40', '', NULL),
(10, 'Neiva', '2018-12-14', 'La Entidad compartio, en encuentro de ganaderos del pais, las acciones y apuestas que realiza para fortalecer al campo colombiano, especialmente en el sector pecuario, donde se busca fomentar la produccion ganadera eficiente y rentable.', 'SENA ha destinado $20 mil millones para creacion de empresas agropecuarias', 'ganaderia.jpg', 1, '2018-12-08 15:57:17', '2018-12-08 15:56:24', '', NULL),
(11, 'Neiva', '2018-12-17', 'Carla Bacigalupo, ministra del Trabajo, Empleo y Seguridad Social de Paraguay, junto con su equipo parlamentario, conocen algunos centros de la Entidad y sus modelos de gestion.', 'Delegacion Paraguaya efectua visita exploratoria al SENA', 'MinParaguay.jpg', 1, '2018-12-08 15:56:24', '2018-12-08 15:56:24', '', NULL),
(12, 'Neiva', '2018-12-19', 'Fue lo que se evidencio durante el decimosexto taller Construyendo Pais, dirigido por el Presidente de la Republica y realizado en Cucuta (Norte de Santander).', '\r\nCultura emprendedora en la que el SENA es ficha clave', 'fichaClave.jpg', 2, '2018-12-08 16:00:54', '2018-12-08 16:00:54', '', NULL),
(13, 'Neiva', '2018-12-19', 'Podran participar los profesionales que tengan meritos academicos y amplia experiencia pedagogica y laboral.', 'Se abrira proceso meritocratico para contratar a mas de 20 mil instructores SENA', 'contratar.jpg', 1, '2018-12-08 16:12:22', '2018-12-08 16:12:22', '', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nutricion`
--

CREATE TABLE `nutricion` (
  `id_nutricion` int(11) NOT NULL,
  `titulo_nutricion` varchar(50) NOT NULL,
  `descripcion_nutricion` varchar(2000) NOT NULL,
  `imagen_nutricion` varchar(50) NOT NULL,
  `id_tiponutricion` int(11) NOT NULL,
  `id_imc` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `nutricion`
--

INSERT INTO `nutricion` (`id_nutricion`, `titulo_nutricion`, `descripcion_nutricion`, `imagen_nutricion`, `id_tiponutricion`, `id_imc`) VALUES
(1, 'ese mix2aaa', 'lorem', 'eso.jpg', 1, 1),
(2, 'ese mi2', 'ksjhdashdashduhasudhasiudhasuidhasuidhas', '', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pais`
--

CREATE TABLE `pais` (
  `id_pais` int(11) NOT NULL,
  `descripcion_pais` varchar(40) NOT NULL,
  `fechacreacion_pais` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fechamodificacion_pais` datetime DEFAULT CURRENT_TIMESTAMP,
  `usuariocreacion_pais` varchar(20) NOT NULL,
  `usuariomodificacion_pais` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pais`
--

INSERT INTO `pais` (`id_pais`, `descripcion_pais`, `fechacreacion_pais`, `fechamodificacion_pais`, `usuariocreacion_pais`, `usuariomodificacion_pais`) VALUES
(1, 'Colombia', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

CREATE TABLE `persona` (
  `id_persona` int(11) NOT NULL,
  `nombre_persona` varchar(50) NOT NULL,
  `apellido_persona` varchar(50) NOT NULL,
  `edad_persona` char(3) NOT NULL,
  `correo_persona` varchar(100) NOT NULL,
  `documento_persona` int(11) NOT NULL,
  `id_tipousuario` int(11) NOT NULL,
  `eps_persona` varchar(20) NOT NULL,
  `rh_persona` char(2) NOT NULL,
  `estado_persona` tinyint(1) NOT NULL,
  `contrasena_persona` text NOT NULL,
  `google_persona` tinyint(1) NOT NULL,
  `fechacreacion_persona` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fechamodificacion_persona` datetime DEFAULT CURRENT_TIMESTAMP,
  `usuariocreacion_persona` varchar(20) NOT NULL,
  `usuariomodificacion_persona` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `persona`
--

INSERT INTO `persona` (`id_persona`, `nombre_persona`, `apellido_persona`, `edad_persona`, `correo_persona`, `documento_persona`, `id_tipousuario`, `eps_persona`, `rh_persona`, `estado_persona`, `contrasena_persona`, `google_persona`, `fechacreacion_persona`, `fechamodificacion_persona`, `usuariocreacion_persona`, `usuariomodificacion_persona`) VALUES
(1, 'Cesar', '1e', '17', 'caca@dasdas.ccc', 100, 0, 'sadasd', 'O+', 1, '', 0, '2018-12-05 17:43:23', '2018-12-05 17:43:23', '', NULL),
(2, 'hola', '1e', '2', 'caca@dasas.cc', 2, 0, '2', 'A+', 1, '', 0, '2018-12-05 17:48:28', '2018-12-05 17:48:28', '', NULL),
(3, 'hola', '1e', '', 'caca@misena.edu.co', 0, 0, 'sadasd', 'ii', 0, '', 0, '2018-12-05 18:54:48', '2018-12-05 18:54:48', '', NULL),
(4, 'hola', '1e', '', 'caa@misena.edu.co', 0, 0, 'sadasd', 'ii', 0, '', 0, '2018-12-05 18:55:29', '2018-12-05 18:55:29', '', NULL),
(5, 'hola', '1e', '', 'ca@misena.edu.co', 0, 0, 'sadasd', 'ii', 0, '', 0, '2018-12-05 18:56:00', '2018-12-05 18:56:00', '', NULL),
(6, 'hola', '1e', '', 'a@misena.edu.co', 0, 0, 'sadasd', 'ii', 0, '', 0, '2018-12-05 18:56:31', '2018-12-05 18:56:31', '', NULL),
(7, 'asdasd', 'asdas', '', 'asdasda@misena.edu.co', 0, 0, 'asdas', 'as', 0, '', 0, '2018-12-05 19:02:35', '2018-12-05 19:02:35', '', NULL),
(8, 'cesar', 'sadas', '17', '', 1042, 0, 'ssss', 'O+', 0, '', 0, '2018-12-05 20:36:20', '2018-12-05 20:36:20', '', NULL),
(9, 'cesar', 'sadas', '2', '', 2, 0, '2', '2', 0, '', 0, '2018-12-05 20:38:53', '2018-12-05 20:38:53', '', NULL),
(10, 'asdasd', 'asdasd', '2', '', 2, 0, '2', '2', 0, '', 0, '2018-12-05 20:39:08', '2018-12-05 20:39:08', '', NULL),
(17, 'dasd', 'asdasd', '22', '', 22, 0, '22', '22', 0, '', 0, '2018-12-05 20:39:43', '2018-12-05 20:39:43', '', NULL),
(19, 'cesar augusto', 'cassa', '22', '', 22, 0, '22', '22', 0, '', 0, '2018-12-08 17:18:42', '2018-12-05 20:40:46', '', NULL),
(20, 'sss', 'ss', '22', '', 22, 0, '22', '22', 0, '', 0, '2018-12-05 20:41:37', '2018-12-05 20:41:37', '', NULL),
(22, 'asdasd', 'asdas', '22', '', 22, 0, '222', '22', 0, '', 0, '2018-12-05 20:45:55', '2018-12-05 20:45:55', '', NULL),
(25, 'holi', 'wsws', '22', '', 22, 0, '22', '22', 0, '', 0, '2018-12-05 20:49:04', '2018-12-05 20:49:04', '', NULL),
(26, 'hola', '1e', '', 'hola@misena.edu.co', 0, 0, 'sadasd', 'i', 0, '', 0, '2018-12-09 18:05:37', '2018-12-09 18:05:37', '', NULL),
(27, 'hola', '1e', '', 'holii@misena.edu.oc', 0, 0, 'sadasd', 'as', 0, '', 0, '2018-12-09 18:15:33', '2018-12-09 18:15:33', '', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prestamo`
--

CREATE TABLE `prestamo` (
  `id_prestamo` int(11) NOT NULL,
  `tipodocumento_prestamo` varchar(20) NOT NULL,
  `documento_prestamo` int(11) NOT NULL,
  `descripcion_prestamo` varchar(1000) NOT NULL,
  `fechaentrega_prestamo` datetime NOT NULL,
  `ficha_prestamo` int(11) NOT NULL,
  `id_sede` int(11) NOT NULL,
  `recibido_prestamo` tinyint(1) NOT NULL,
  `novedad_prestamo` varchar(1000) DEFAULT NULL,
  `fecharecibido_prestamo` datetime DEFAULT NULL,
  `fechacreacion_prestamo` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fechamodificacion_prestamo` datetime DEFAULT CURRENT_TIMESTAMP,
  `usuariocreacion_prestamo` varchar(20) NOT NULL,
  `usuariomodificacion_prestamo` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `prestamo`
--

INSERT INTO `prestamo` (`id_prestamo`, `tipodocumento_prestamo`, `documento_prestamo`, `descripcion_prestamo`, `fechaentrega_prestamo`, `ficha_prestamo`, `id_sede`, `recibido_prestamo`, `novedad_prestamo`, `fecharecibido_prestamo`, `fechacreacion_prestamo`, `fechamodificacion_prestamo`, `usuariocreacion_prestamo`, `usuariomodificacion_prestamo`) VALUES
(1, 'Cedula', 100, 'fsdfdas', '2018-12-02 18:36:21', 545, 1, 1, 'hola', '2018-12-02 18:37:58', '2018-12-02 18:37:58', '2018-12-02 18:36:21', '', NULL),
(2, 'Tarjeta de Identidad', 100, 'fsdfdas', '2018-12-02 18:36:52', 545, 1, 1, '', '2018-12-02 18:38:23', '2018-12-02 18:38:23', '2018-12-02 18:36:52', '', NULL),
(3, 'Tarjeta de Identidad', 10, 'fsdfdas', '2018-12-02 18:37:08', 545, 1, 0, NULL, NULL, '2018-12-02 18:37:08', '2018-12-02 18:37:08', '', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `programaformacion`
--

CREATE TABLE `programaformacion` (
  `id_programaformacion` int(11) NOT NULL,
  `codigo_programaformacion` int(20) NOT NULL,
  `descripcion_programaformacion` varchar(50) NOT NULL,
  `duracion_programaformacion` tinyint(3) NOT NULL,
  `id_nivelprograma` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `programaformacion`
--

INSERT INTO `programaformacion` (`id_programaformacion`, `codigo_programaformacion`, `descripcion_programaformacion`, `duracion_programaformacion`, `id_nivelprograma`) VALUES
(1, 1, 'hola', 14, 1),
(2, 12, 'assa', 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `programa_formacion`
--

CREATE TABLE `programa_formacion` (
  `id_programa_formacion` int(11) NOT NULL,
  `codigo` int(11) NOT NULL,
  `descripcion` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `duracion` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `id_nivel_programa` int(11) NOT NULL,
  `estado` bit(1) NOT NULL,
  `id_usuario_creacion` int(11) NOT NULL,
  `id_usuario_modificacion` int(11) NOT NULL,
  `fecha_creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fecha_modificacion` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recibidoentrega`
--

CREATE TABLE `recibidoentrega` (
  `id_recibidoentrega` int(11) NOT NULL,
  `id_persona` int(11) NOT NULL,
  `codigorecibido_chequeoinventario` int(11) NOT NULL,
  `fecharecibido_recibidoentrega` datetime NOT NULL,
  `codigoentrega_chequeoinventario` int(11) NOT NULL,
  `fechaentrega_recibidoentrega` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `region`
--

CREATE TABLE `region` (
  `id_region` int(11) NOT NULL,
  `descripcion_region` varchar(40) NOT NULL,
  `estado_region` tinyint(1) NOT NULL,
  `imagen_region` text NOT NULL,
  `id_pais` int(11) NOT NULL,
  `fechacreacion_region` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fechamodificacion_region` datetime DEFAULT CURRENT_TIMESTAMP,
  `usuariocreacion_region` varchar(20) NOT NULL,
  `usuariomodificacion_region` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `region`
--

INSERT INTO `region` (`id_region`, `descripcion_region`, `estado_region`, `imagen_region`, `id_pais`, `fechacreacion_region`, `fechamodificacion_region`, `usuariocreacion_region`, `usuariomodificacion_region`) VALUES
(1, 'Caribe', 0, '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(2, 'Andin', 0, '', 1, '2018-12-05 21:29:02', '0000-00-00 00:00:00', '', ''),
(3, 'Pacifico', 0, '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(4, 'Orinoquia', 0, '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(5, 'Amazonia', 0, '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(6, 'asdasd', 1, 'descarga.jpg', 1, '2018-12-02 18:55:06', '2018-12-02 18:52:54', '', NULL),
(7, 'dasdasdasdasdasdasd', 0, '4.', 1, '2018-12-02 18:55:14', '2018-12-02 18:55:14', '', NULL),
(8, '2', 1, '45384774_299360074236424_8592526042122420224_n.jpg', 1, '2018-12-02 18:56:35', '2018-12-02 18:56:35', '', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `regional`
--

CREATE TABLE `regional` (
  `id_regional` int(11) NOT NULL,
  `regional_regional` varchar(20) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `departamento_regional` varchar(20) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `subdirector_regional` varchar(20) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `estado_regional` varchar(20) COLLATE utf8mb4_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `regional`
--

INSERT INTO `regional` (`id_regional`, `regional_regional`, `departamento_regional`, `subdirector_regional`, `estado_regional`) VALUES
(4, 'xs', 'Atlï¿½ntico', 'Arturo Arango Santos', '1'),
(5, 'xss', 'Arturo Arango Santos', '', '1'),
(6, 'asdasdas', 'Atlï¿½ntico', 'Arturo Arango Santos', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reporterutina`
--

CREATE TABLE `reporterutina` (
  `id_reporterutina` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

CREATE TABLE `rol` (
  `id_rol` int(11) NOT NULL,
  `descripcion_rol` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`id_rol`, `descripcion_rol`) VALUES
(1, 'hola'),
(2, 'sdasd');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rutina`
--

CREATE TABLE `rutina` (
  `id_rutina` int(11) NOT NULL,
  `titulo_rutina` varchar(40) NOT NULL,
  `descripcion_rutina` varchar(2000) NOT NULL,
  `imagen_rutina` varchar(50) NOT NULL,
  `id_tiporutina` int(11) NOT NULL,
  `id_imc` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rutina`
--

INSERT INTO `rutina` (`id_rutina`, `titulo_rutina`, `descripcion_rutina`, `imagen_rutina`, `id_tiporutina`, `id_imc`) VALUES
(1, 'ese mismo x2', 'hola', 'eso.jpg', 3, 2),
(2, 'ese mismo', 'sss', '1e.png', 3, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sede`
--

CREATE TABLE `sede` (
  `id_sede` int(11) NOT NULL,
  `id_municipio` int(11) NOT NULL,
  `direccion_sede` varchar(40) NOT NULL,
  `nombre_sede` varchar(100) NOT NULL,
  `img_sede` text NOT NULL,
  `estado_sede` tinyint(1) NOT NULL,
  `fechacreacion_sede` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fechamodificacion_sede` datetime DEFAULT CURRENT_TIMESTAMP,
  `usuariocreacion_sede` varchar(20) NOT NULL,
  `usuariomodificacion_sede` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `sede`
--

INSERT INTO `sede` (`id_sede`, `id_municipio`, `direccion_sede`, `nombre_sede`, `img_sede`, `estado_sede`, `fechacreacion_sede`, `fechamodificacion_sede`, `usuariocreacion_sede`, `usuariomodificacion_sede`) VALUES
(1, 72, 'sss', 'Industrial', '0', 0, '2018-12-09 16:20:41', '0000-00-00 00:00:00', 'sss', 'sss'),
(2, 2, 'as22', 'Comercial', '7937452e5a43986264f57455fa8c4ed4.jpg', 0, '2018-12-09 16:20:56', '2018-12-05 18:29:57', '', NULL),
(3, 1, 'as22', 'hol', '7937452e5a43986264f57455fa8c4ed4.jpg', 0, '2018-12-05 18:30:12', '2018-12-05 18:30:12', '', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tiponutricion`
--

CREATE TABLE `tiponutricion` (
  `id_nutricion` int(11) NOT NULL,
  `descripcion_nutricion` varchar(50) NOT NULL,
  `fechacreacion_tiponutricion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fechamodificacion_tiponutricion` datetime DEFAULT CURRENT_TIMESTAMP,
  `usuariocreacion_tiponutricion` varchar(20) NOT NULL,
  `usuariomodificacion_tiponutricion` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tiponutricion`
--

INSERT INTO `tiponutricion` (`id_nutricion`, `descripcion_nutricion`, `fechacreacion_tiponutricion`, `fechamodificacion_tiponutricion`, `usuariocreacion_tiponutricion`, `usuariomodificacion_tiponutricion`) VALUES
(1, 'Nutrientes', '2018-12-09 17:39:05', '2018-12-09 17:39:05', '', NULL),
(2, 'Alimentos', '2018-12-09 17:39:05', '2018-12-09 17:39:05', '', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tiporutina`
--

CREATE TABLE `tiporutina` (
  `id_tiporutina` int(11) NOT NULL,
  `descripcion_tiporutina` varchar(50) NOT NULL,
  `fechacreacion_tiporutina` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fechamodificacion_tiporutina` datetime DEFAULT CURRENT_TIMESTAMP,
  `usuariocreacion_tiporutina` varchar(20) NOT NULL,
  `usuariomodificacion_tiporutina` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tiporutina`
--

INSERT INTO `tiporutina` (`id_tiporutina`, `descripcion_tiporutina`, `fechacreacion_tiporutina`, `fechamodificacion_tiporutina`, `usuariocreacion_tiporutina`, `usuariomodificacion_tiporutina`) VALUES
(3, 'hola', '2018-12-05 21:43:45', '2018-12-05 21:43:45', '', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipousuario`
--

CREATE TABLE `tipousuario` (
  `id_tipousuario` int(11) NOT NULL,
  `descripcion_tipousuario` varchar(40) NOT NULL,
  `fechacreacion_tipousuario` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fechamodificacion_tipousuario` datetime DEFAULT CURRENT_TIMESTAMP,
  `usuariocreacion_tipousuario` varchar(20) NOT NULL,
  `usuariomodificacion_tipousuario` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id_usuario` int(11) NOT NULL,
  `id_persona` int(11) NOT NULL,
  `id_tipousuario` int(11) NOT NULL,
  `contrasena_usuario` varchar(260) NOT NULL,
  `google_usuario` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id_usuario`, `id_persona`, `id_tipousuario`, `contrasena_usuario`, `google_usuario`) VALUES
(1, 6, 1, '1', 0),
(2, 7, 1, '1', 0),
(3, 26, 1, 'c81e728d9d4c2f636f067f89cc14862c', 0),
(4, 27, 1, 'c81e728d9d4c2f636f067f89cc14862c', 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `actividad`
--
ALTER TABLE `actividad`
  ADD PRIMARY KEY (`id_actividad`),
  ADD KEY `id_gimnasio` (`id_gimnasio`),
  ADD KEY `id_tipo` (`id_tipo`),
  ADD KEY `id_fecha` (`fecha`);

--
-- Indices de la tabla `ambiente`
--
ALTER TABLE `ambiente`
  ADD PRIMARY KEY (`id_ambiente`);

--
-- Indices de la tabla `aprendiz`
--
ALTER TABLE `aprendiz`
  ADD PRIMARY KEY (`id_aprendiz`),
  ADD KEY `id_persona` (`id_persona`);

--
-- Indices de la tabla `aprendizficha`
--
ALTER TABLE `aprendizficha`
  ADD PRIMARY KEY (`id_aprendizficha`),
  ADD UNIQUE KEY `id_aprendiz_2` (`id_aprendiz`),
  ADD KEY `id_aprendiz` (`id_aprendiz`,`id_ficha`),
  ADD KEY `id_ficha` (`id_ficha`);

--
-- Indices de la tabla `aprendiz_ficha`
--
ALTER TABLE `aprendiz_ficha`
  ADD PRIMARY KEY (`id_aprendiz_fichca`),
  ADD KEY `id_ficha` (`id_ficha`),
  ADD KEY `id_aprendiz` (`id_aprendiz`);

--
-- Indices de la tabla `aseguradora`
--
ALTER TABLE `aseguradora`
  ADD PRIMARY KEY (`id_aseguradora`);

--
-- Indices de la tabla `centro_formacion`
--
ALTER TABLE `centro_formacion`
  ADD PRIMARY KEY (`id_centro_formacion`),
  ADD KEY `id_aseguradora` (`id_aseguradora`),
  ADD KEY `id_ciudad` (`id_ciudad`),
  ADD KEY `id_regional` (`id_regional`);

--
-- Indices de la tabla `chequeoinventario`
--
ALTER TABLE `chequeoinventario`
  ADD PRIMARY KEY (`id_chequeoinventario`),
  ADD KEY `id_inventariogym` (`id_inventariogym`,`id_estadoproducto`),
  ADD KEY `codigo_chequeoinventario` (`codigo_chequeoinventario`),
  ADD KEY `id_estado` (`id_estadoproducto`);

--
-- Indices de la tabla `chequeonovedad`
--
ALTER TABLE `chequeonovedad`
  ADD PRIMARY KEY (`id_chequeonovedad`),
  ADD KEY `codigo_chequeoinventario` (`codigo_chequeoinventario`);

--
-- Indices de la tabla `ciudad`
--
ALTER TABLE `ciudad`
  ADD PRIMARY KEY (`id_ciudad`),
  ADD KEY `id_departamento` (`id_departamento`);

--
-- Indices de la tabla `departamento`
--
ALTER TABLE `departamento`
  ADD PRIMARY KEY (`id_departamento`),
  ADD KEY `id_region` (`id_region`);

--
-- Indices de la tabla `eps`
--
ALTER TABLE `eps`
  ADD PRIMARY KEY (`id_eps`);

--
-- Indices de la tabla `equipo`
--
ALTER TABLE `equipo`
  ADD PRIMARY KEY (`id_equipo`),
  ADD KEY `id_gimnasio` (`id_gimnasio`),
  ADD KEY `id_marca` (`id_marca`);

--
-- Indices de la tabla `estadoproducto`
--
ALTER TABLE `estadoproducto`
  ADD PRIMARY KEY (`id_estadoproducto`);

--
-- Indices de la tabla `ficha`
--
ALTER TABLE `ficha`
  ADD PRIMARY KEY (`id_ficha`),
  ADD UNIQUE KEY `ficha_ficha` (`ficha_ficha`),
  ADD KEY `id_ambiente` (`id_ambiente`,`id_programa_formacion`),
  ADD KEY `id_programa_formacion` (`id_programa_formacion`);

--
-- Indices de la tabla `gimnasio`
--
ALTER TABLE `gimnasio`
  ADD PRIMARY KEY (`id_gimnasio`),
  ADD KEY `id_ciudad` (`id_ciudad`),
  ADD KEY `id_sede` (`id_sede`);

--
-- Indices de la tabla `imc`
--
ALTER TABLE `imc`
  ADD PRIMARY KEY (`id_imc`);

--
-- Indices de la tabla `instructor`
--
ALTER TABLE `instructor`
  ADD PRIMARY KEY (`id_instructor`),
  ADD UNIQUE KEY `id_persona` (`id_persona`);

--
-- Indices de la tabla `inventariogym`
--
ALTER TABLE `inventariogym`
  ADD PRIMARY KEY (`id_inventariogym`),
  ADD KEY `id_sede` (`id_sede`),
  ADD KEY `id_estadoproducto` (`id_estadoproducto`);

--
-- Indices de la tabla `municipio`
--
ALTER TABLE `municipio`
  ADD PRIMARY KEY (`id_municipio`),
  ADD KEY `id_departamento` (`id_departamento`);

--
-- Indices de la tabla `nivelprograma`
--
ALTER TABLE `nivelprograma`
  ADD PRIMARY KEY (`id_nivelprograma`);

--
-- Indices de la tabla `noticia`
--
ALTER TABLE `noticia`
  ADD PRIMARY KEY (`id_noticia`),
  ADD KEY `id_sede` (`id_sede`);

--
-- Indices de la tabla `nutricion`
--
ALTER TABLE `nutricion`
  ADD PRIMARY KEY (`id_nutricion`),
  ADD KEY `id_tiponutricion` (`id_tiponutricion`,`id_imc`),
  ADD KEY `id_imc` (`id_imc`);

--
-- Indices de la tabla `pais`
--
ALTER TABLE `pais`
  ADD PRIMARY KEY (`id_pais`);

--
-- Indices de la tabla `persona`
--
ALTER TABLE `persona`
  ADD PRIMARY KEY (`id_persona`),
  ADD KEY `id_tipousuario` (`id_tipousuario`);

--
-- Indices de la tabla `prestamo`
--
ALTER TABLE `prestamo`
  ADD PRIMARY KEY (`id_prestamo`),
  ADD KEY `id_sede` (`id_sede`);

--
-- Indices de la tabla `programaformacion`
--
ALTER TABLE `programaformacion`
  ADD PRIMARY KEY (`id_programaformacion`),
  ADD KEY `id_nivelprograma` (`id_nivelprograma`);

--
-- Indices de la tabla `recibidoentrega`
--
ALTER TABLE `recibidoentrega`
  ADD PRIMARY KEY (`id_recibidoentrega`),
  ADD KEY `id_persona` (`id_persona`,`codigorecibido_chequeoinventario`,`codigoentrega_chequeoinventario`),
  ADD KEY `codigorecibido_chequeoinventario` (`codigorecibido_chequeoinventario`),
  ADD KEY `codigoentrega_chequeoinventario` (`codigoentrega_chequeoinventario`);

--
-- Indices de la tabla `region`
--
ALTER TABLE `region`
  ADD PRIMARY KEY (`id_region`),
  ADD KEY `id_pais` (`id_pais`);

--
-- Indices de la tabla `regional`
--
ALTER TABLE `regional`
  ADD PRIMARY KEY (`id_regional`);

--
-- Indices de la tabla `reporterutina`
--
ALTER TABLE `reporterutina`
  ADD PRIMARY KEY (`id_reporterutina`);

--
-- Indices de la tabla `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`id_rol`);

--
-- Indices de la tabla `rutina`
--
ALTER TABLE `rutina`
  ADD PRIMARY KEY (`id_rutina`),
  ADD KEY `id_tiporutina` (`id_tiporutina`,`id_imc`),
  ADD KEY `id_imc` (`id_imc`);

--
-- Indices de la tabla `sede`
--
ALTER TABLE `sede`
  ADD PRIMARY KEY (`id_sede`),
  ADD KEY `id_municipio` (`id_municipio`);

--
-- Indices de la tabla `tiponutricion`
--
ALTER TABLE `tiponutricion`
  ADD PRIMARY KEY (`id_nutricion`);

--
-- Indices de la tabla `tiporutina`
--
ALTER TABLE `tiporutina`
  ADD PRIMARY KEY (`id_tiporutina`);

--
-- Indices de la tabla `tipousuario`
--
ALTER TABLE `tipousuario`
  ADD PRIMARY KEY (`id_tipousuario`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_usuario`),
  ADD KEY `id_tipousuario` (`id_tipousuario`),
  ADD KEY `id_persona` (`id_persona`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `ambiente`
--
ALTER TABLE `ambiente`
  MODIFY `id_ambiente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `aprendiz`
--
ALTER TABLE `aprendiz`
  MODIFY `id_aprendiz` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `aprendizficha`
--
ALTER TABLE `aprendizficha`
  MODIFY `id_aprendizficha` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `chequeoinventario`
--
ALTER TABLE `chequeoinventario`
  MODIFY `id_chequeoinventario` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `chequeonovedad`
--
ALTER TABLE `chequeonovedad`
  MODIFY `id_chequeonovedad` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `departamento`
--
ALTER TABLE `departamento`
  MODIFY `id_departamento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT de la tabla `estadoproducto`
--
ALTER TABLE `estadoproducto`
  MODIFY `id_estadoproducto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `ficha`
--
ALTER TABLE `ficha`
  MODIFY `id_ficha` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `imc`
--
ALTER TABLE `imc`
  MODIFY `id_imc` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `instructor`
--
ALTER TABLE `instructor`
  MODIFY `id_instructor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `inventariogym`
--
ALTER TABLE `inventariogym`
  MODIFY `id_inventariogym` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `municipio`
--
ALTER TABLE `municipio`
  MODIFY `id_municipio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1098;

--
-- AUTO_INCREMENT de la tabla `nivelprograma`
--
ALTER TABLE `nivelprograma`
  MODIFY `id_nivelprograma` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `noticia`
--
ALTER TABLE `noticia`
  MODIFY `id_noticia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `nutricion`
--
ALTER TABLE `nutricion`
  MODIFY `id_nutricion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `pais`
--
ALTER TABLE `pais`
  MODIFY `id_pais` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `persona`
--
ALTER TABLE `persona`
  MODIFY `id_persona` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT de la tabla `prestamo`
--
ALTER TABLE `prestamo`
  MODIFY `id_prestamo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `programaformacion`
--
ALTER TABLE `programaformacion`
  MODIFY `id_programaformacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `recibidoentrega`
--
ALTER TABLE `recibidoentrega`
  MODIFY `id_recibidoentrega` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `region`
--
ALTER TABLE `region`
  MODIFY `id_region` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `regional`
--
ALTER TABLE `regional`
  MODIFY `id_regional` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `reporterutina`
--
ALTER TABLE `reporterutina`
  MODIFY `id_reporterutina` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `rol`
--
ALTER TABLE `rol`
  MODIFY `id_rol` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `rutina`
--
ALTER TABLE `rutina`
  MODIFY `id_rutina` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `sede`
--
ALTER TABLE `sede`
  MODIFY `id_sede` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tiponutricion`
--
ALTER TABLE `tiponutricion`
  MODIFY `id_nutricion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tiporutina`
--
ALTER TABLE `tiporutina`
  MODIFY `id_tiporutina` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tipousuario`
--
ALTER TABLE `tipousuario`
  MODIFY `id_tipousuario` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `aprendiz`
--
ALTER TABLE `aprendiz`
  ADD CONSTRAINT `aprendiz_ibfk_1` FOREIGN KEY (`id_persona`) REFERENCES `persona` (`id_persona`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `aprendizficha`
--
ALTER TABLE `aprendizficha`
  ADD CONSTRAINT `aprendizficha_ibfk_1` FOREIGN KEY (`id_ficha`) REFERENCES `ficha` (`id_ficha`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `aprendizficha_ibfk_2` FOREIGN KEY (`id_aprendiz`) REFERENCES `aprendiz` (`id_aprendiz`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `chequeoinventario`
--
ALTER TABLE `chequeoinventario`
  ADD CONSTRAINT `chequeoinventario_ibfk_1` FOREIGN KEY (`id_inventariogym`) REFERENCES `inventariogym` (`id_inventariogym`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `chequeoinventario_ibfk_2` FOREIGN KEY (`id_estadoproducto`) REFERENCES `estadoproducto` (`id_estadoproducto`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `chequeonovedad`
--
ALTER TABLE `chequeonovedad`
  ADD CONSTRAINT `chequeonovedad_ibfk_1` FOREIGN KEY (`codigo_chequeoinventario`) REFERENCES `chequeoinventario` (`codigo_chequeoinventario`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `departamento`
--
ALTER TABLE `departamento`
  ADD CONSTRAINT `departamento_ibfk_1` FOREIGN KEY (`id_region`) REFERENCES `region` (`id_region`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `ficha`
--
ALTER TABLE `ficha`
  ADD CONSTRAINT `ficha_ibfk_1` FOREIGN KEY (`id_ambiente`) REFERENCES `ambiente` (`id_ambiente`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ficha_ibfk_2` FOREIGN KEY (`id_programa_formacion`) REFERENCES `programaformacion` (`id_programaformacion`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `instructor`
--
ALTER TABLE `instructor`
  ADD CONSTRAINT `instructor_ibfk_1` FOREIGN KEY (`id_persona`) REFERENCES `persona` (`id_persona`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `inventariogym`
--
ALTER TABLE `inventariogym`
  ADD CONSTRAINT `inventariogym_ibfk_1` FOREIGN KEY (`id_estadoproducto`) REFERENCES `estadoproducto` (`id_estadoproducto`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `inventariogym_ibfk_2` FOREIGN KEY (`id_sede`) REFERENCES `sede` (`id_sede`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `municipio`
--
ALTER TABLE `municipio`
  ADD CONSTRAINT `municipio_ibfk_1` FOREIGN KEY (`id_departamento`) REFERENCES `departamento` (`id_departamento`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `noticia`
--
ALTER TABLE `noticia`
  ADD CONSTRAINT `noticia_ibfk_1` FOREIGN KEY (`id_sede`) REFERENCES `sede` (`id_sede`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `nutricion`
--
ALTER TABLE `nutricion`
  ADD CONSTRAINT `nutricion_ibfk_1` FOREIGN KEY (`id_imc`) REFERENCES `imc` (`id_imc`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `nutricion_ibfk_2` FOREIGN KEY (`id_tiponutricion`) REFERENCES `tiponutricion` (`id_nutricion`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `prestamo`
--
ALTER TABLE `prestamo`
  ADD CONSTRAINT `prestamo_ibfk_1` FOREIGN KEY (`id_sede`) REFERENCES `sede` (`id_sede`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `programaformacion`
--
ALTER TABLE `programaformacion`
  ADD CONSTRAINT `programaformacion_ibfk_1` FOREIGN KEY (`id_nivelprograma`) REFERENCES `nivelprograma` (`id_nivelprograma`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `recibidoentrega`
--
ALTER TABLE `recibidoentrega`
  ADD CONSTRAINT `recibidoentrega_ibfk_1` FOREIGN KEY (`id_persona`) REFERENCES `persona` (`id_persona`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `recibidoentrega_ibfk_2` FOREIGN KEY (`codigorecibido_chequeoinventario`) REFERENCES `chequeoinventario` (`codigo_chequeoinventario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `region`
--
ALTER TABLE `region`
  ADD CONSTRAINT `region_ibfk_1` FOREIGN KEY (`id_pais`) REFERENCES `pais` (`id_pais`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `rutina`
--
ALTER TABLE `rutina`
  ADD CONSTRAINT `rutina_ibfk_1` FOREIGN KEY (`id_imc`) REFERENCES `imc` (`id_imc`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `rutina_ibfk_2` FOREIGN KEY (`id_tiporutina`) REFERENCES `tiporutina` (`id_tiporutina`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `sede`
--
ALTER TABLE `sede`
  ADD CONSTRAINT `sede_ibfk_1` FOREIGN KEY (`id_municipio`) REFERENCES `municipio` (`id_municipio`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`id_persona`) REFERENCES `persona` (`id_persona`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
