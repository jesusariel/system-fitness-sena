 function guardar(accion,pos){
    var json = {
        'descripcion': $('#txtNombre').val(),
        'accion': accion,
        'id': pos
        };
      $.ajax({
        url: '../../../Controlador/gymTipoRutController.php',
        type: 'POST',
        dataType: 'json',
        data: json,
        success: function (data){
            alert(data["mensaje"]);
            if(data['exito'] == 1){
                limpiar();
                if(accion == 1){
                 // Obtenemos el total de columnas (tr) del id "tabla"
                 var trs=$("table tr").length;
                 var nuevaFila="<tr id='pos" + (trs) +"'>";
                 // añadimos las columnas
                 nuevaFila+="<td>" + json['descripcion'] +"</td>";
                 nuevaFila+="<td><input type='button' value='Editar' class='btnEditar' onclick='litarEditar("+ (trs) +")'><input type='button' value='Eliminar' class='btnEliminar' onclick='eliminar("+ (trs) +")'></td>";
                 nuevaFila+="</tr>";
                 $("table").append(nuevaFila);
                }else{
                 $('#pos' + pos).children('td')[0].innerHTML = json['descripcion'];
                }
            }
        }
      });
  }
  
  function litarEditar(id){
   json = {
    'id': id,
    'accion': 2
   }
      $.ajax({
        url: '../../../Controlador/gymTipoRutController.php',
        type: 'POST',
        dataType: 'json',
        data: json,
        success: function (data){
            $("input[id='txtNombre']").val(data['datos']['descripcion_tiporutina']);
            $("#submit").attr("value","Editar");
            $("#form").attr("action", "javascript: guardar(3," + id + ")");
        }
      });
  }
  
  function limpiar(){
   $("form")[1].reset();
   $("#submit").attr("value","Agregar");
   $("#form").attr("action", "javascript: guardar(1,0);");
  }
  
  function eliminar(id){
   if(confirm("Deseas eliminarlo?")){
    json = {
    'id': id,
    'accion': 4
   }
      $.ajax({
        url: '../../../Controlador/gymTipoRutController.php',
        type: 'POST',
        dataType: 'json',
        data: json,
        success: function (data){
            if(data['exito'] == 1){
                $('#pos' + id).remove();
                limpiar();
            }
        }
      });
   }
  }