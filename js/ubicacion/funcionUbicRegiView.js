 function guardar(accion,pos){
    var json = new FormData($('form')[1]);
    json.append('accion',accion);
    json.append('id',pos);
    var pais = $('select[name="slcPais"] option:selected').text();
      $.ajax({
        url: '../../Controlador/ubicacion/ubicacioRegiController.php',
        type: 'POST',
        dataType: 'json',
        data: json,
        contentType: false,
        cache: false,
        processData:false,
        success: function (data){
            alert(data["mensaje"]);
            if(data['exito'] == 1){
                limpiar();
                if(accion == 1){
                 // Obtenemos el total de columnas (tr) del id "tabla"
                 var trs=$("table tr").length;
                 var nuevaFila="<tr id='pos" + (trs) +"'>";
                 // añadimos las columnas
                 nuevaFila+="<td>" + pais +"</td>";
                 nuevaFila+="<td>" + json.get('txtNombreUbicacion') + "</td>";
                 nuevaFila+="<td><input type='button' value='Editar' class='btnEditar' onclick='litarEditar("+ (trs) +")'><input type='button' value='Eliminar' class='btnEliminar' onclick='eliminar("+ (trs) +")'></td>";
                 nuevaFila+="</tr>";
                 $("table").append(nuevaFila);
                }else{
                 $('#pos' + pos).children('td')[0].innerHTML = pais;
                 $('#pos' + pos).children('td')[1].innerHTML = json.get('txtNombreUbicacion');
                }
            }
        }
      });
  }
  
  function litarEditar(id){
   var form = new FormData($('form')[1]);
   json = {
    'id': id,
    'accion': 2
   }
      $.ajax({
        url: '../../Controlador/ubicacion/ubicacioRegiController.php',
        type: 'POST',
        dataType: 'json',
        data: json,
        success: function (data){
            $("input[name='txtNombreUbicacion']").val(data['datos']['descripcion_region']);
            $("select[name='slcPais']").val(data['datos']['id_pais']);
            $("select[name='slcEstado']").val(data['datos']['estado_region']);
            $("#submit").attr("value","Editar");
            $("#form").attr("action", "javascript: guardar(3," + id + ")");
            $("input[name='filImagen']").removeAttr("required");
            $(".Titulo p").attr("style","display: block");
            $("#form img").attr("style","display: block");
            $("#form img").attr("src","../../img/Ubicacion/"+ data['datos']['imagen_region']);
        }
      });
  }
  
  function limpiar(){
   $("form")[1].reset();
   $("#submit").attr("value","Agregar");
   $(".Titulo p").attr("style","display: none");
   $("#form img").attr("style","display: none");
   $("#form").attr("action", "javascript: guardar(1,0);");
  }
  
  function eliminar(id){
   if(confirm("Deseas eliminar esta región?")){
    json = {
    'id': id,
    'accion': 4
   }
      $.ajax({
        url: '../../Controlador/ubicacion/ubicacioRegiController.php',
        type: 'POST',
        dataType: 'json',
        data: json,
        success: function (data){
            //alert(data["mensaje"]);
            if(data['exito'] == 1){
                $('#pos' + id).remove();
                limpiar();
            }
        }
      });
   }
  }