function guardar(){
    var json = {
      'txtTipoDocumento': $("#txtTipoDocumento").val(),
      'numDocumento': $("#numDocumento").val(),
      'numFicha': $("#numFicha").val(),
      'txtDescripcion': $("#txtDescripcion").val(),
      'accion': 1
    };
      $.ajax({
        url: '../../Controlador/prestamoController.php',
        type: 'POST',
        dataType: 'json',
        data: json,
        success: function (data){
            alert(data["mensaje"]);
            if(data['exito'] == 1){
                $("form")[1].reset();
            }
        }
      });
  }
  
  function consultar(){
    var json = {
      'txtTipoDocumento': $("#txtTipoDocumento").val(),
      'numDocumento': $("#numDocumento").val(),
      'accion': 2
    };
      $.ajax({
        url: '../../Controlador/prestamoController.php',
        type: 'POST',
        dataType: 'json',
        data: json,
        success: function (data){
            if(data['exito'] == 1){
                $("#resultado").attr("style","display: block");
                $("#txtDescripcion").attr("value",data['datos']['descripcion']);
                $("button[class='bien']").attr("onclick","actualizar('" + json['numDocumento'] + "','"+ json['txtTipoDocumento'] +"',true)");
                $("button[class='mal']").attr("onclick","actualizar('" + json['numDocumento'] + "','"+ json['txtTipoDocumento'] +"',false)");
            }else{
              alert(data["mensaje"]);
            }
        }
      });
  }
  
    function actualizar(numDocumento,txtTipoDocumento,recibido){
    var json = {
      'numDocumento': numDocumento,
      'txtTipoDocumento': txtTipoDocumento,
      'txtNovedad': $("#txtNovedad").val(),
      'recibido': recibido,
      'accion': 3
    };
      $.ajax({
        url: '../../Controlador/prestamoController.php',
        type: 'POST',
        dataType: 'json',
        data: json,
        success: function (data){
            alert(data["mensaje"]);
            if(data['exito'] == 1){
                $("form")[1].reset();
                $("form")[2].reset();
                $("#resultado").attr("style","display: none");
            }
        }
      });
  }