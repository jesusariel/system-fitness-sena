
 function guardar(accion,pos){
    json={
			"numFicha": $('#numFicha').val(),
			"txtJornada": $('#txtJornada').val(),
			"datFechaI": $('#datFechaI').val(),
			"datFechaF": $('#datFechaF').val(),
			'accion': accion,
			'id': pos
		}
		for(var indice in json){
		if((json[indice] == "")){
		  alert("Error: Se debe llenar todos los campos");
		  correcto = false;
		  break;
		}else{
		  correcto = true;
		}
	  }
		if(correcto){
			$.ajax({
				url:'../../../Controlador/ficha/fichaController.php',
				type:'POST',
				dataType:'json',
				data: json
			}).done(function(data){
				alert(data["mensaje"]);
            if(data['exito'] == 1){
                limpiar();
                if(accion == 1){
                 // Obtenemos el total de columnas (tr) del id "tabla"
                 var trs=$("table tr").length;
                 var nuevaFila="<tr id='pos" + (trs) +"'>";
                 // añadimos las columnas
                 nuevaFila+="<td>" + json['numFicha'] +"</td>";
                 nuevaFila+="<td>" + json['txtJornada'] + "</td>";
                 nuevaFila+="<td>" + json['datFechaI'] + "</td>";
                 nuevaFila+="<td>" + json['datFechaF'] + "</td>";
                 nuevaFila+="<td><input type='button' value='Editar' class='btnEditar' onclick='litarEditar("+ (trs) +")'><input type='button' value='Eliminar' class='btnEliminar' onclick='eliminar("+ (trs) +")'></td>";
                 nuevaFila+="</tr>";
                 $("table").append(nuevaFila);
                }else{
                 $('#pos' + pos).children('td')[0].innerHTML = json['numFicha'];
                 $('#pos' + pos).children('td')[1].innerHTML = json['txtJornada'];
                 $('#pos' + pos).children('td')[2].innerHTML = json['datFechaI'];
                 $('#pos' + pos).children('td')[3].innerHTML = json['datFechaF'];
                }
            }
			})
		}
  }
  
  function editar(id){
   json = {
    'id': id,
    'accion': 2
   }
      $.ajax({
        url: '../../../Controlador/ficha/fichaController.php',
        type: 'POST',
        dataType: 'json',
        data: json,
        success: function (data){
            $("#numFicha").val(data['datos']['ficha_ficha']);
            $("#txtJornada").val(data['datos']['jornada_ficha']);
            $("#datFechaI").val(data['datos']['fecha_inicio_ficha']);
            $("#datFechaF").val(data['datos']['fecha_fin_ficha']);
            $("#submit").attr("value","Editar");
            $("#form").attr("action", "javascript: guardar(3," + id + ")");
        }
      });
  }
  
  function limpiar(){
   $("form")[1].reset();
   $("#submit").attr("value","Agregar");
   $("#form").attr("action", "javascript: guardar(1,1);");
  }
  
  function eliminar(id){
		if(confirm("Deseas eliminar esta ficha?")){
    json = {
    'id': id,
    'accion': 4
   }
      $.ajax({
        url: '../../../Controlador/ficha/fichaController.php',
        type: 'POST',
        dataType: 'json',
        data: json,
        success: function (data){
            //alert(dataTypea["mensaje"]);
            if(data['exito'] == 1){
                $('#pos' + id).remove();
                limpiar();
            }
        }
      });
   }
  }
