function guardar(accion,pos){
    var json;
		json={
			"txtNombre": $('#txtNombre').val(),
			"numbrNumeroDoc": $('#numbrNumeroDoc').val(),
			"slcTipo": $('#slcTipo').val(),
            "numbrTelefono": $('#numbrTelefono').val(),
			"txtCorreo": $('#txtCorreo').val(),
			'accion':accion,
			'id':pos
		}
      for(var indice in json){
		if((json[indice] == "")){
		  alert("Error: Se debe llenar todos los campos");
		  correcto = false;
		  break;
		}else{
		  correcto = true;
		}
	  }
		if(correcto){
			$.ajax({
			url:'../../Controlador/Seguridad/profesionalController.php',
			type:'POST',
			dataType:'json',
			data: json,
			success: function (data){
				alert(data["mensaje"]);
				if(data['exito'] == 1){
					limpiar();
					if(accion == 1){
					 // Obtenemos el total de columnas (tr) del id "tabla"
					 var trs=$("table tr").length;
					 var nuevaFila="<tr id='pos" + (trs) +"'>";
					 // añadimos las columnas
					 nuevaFila+="<td>" + json['txtNombre'] +"</td>";
                     nuevaFila+="<td>" + json['numbrNumeroDoc'] +"</td>";
					 nuevaFila+="<td>" + json['slcTipo'] + "</td>";
					 nuevaFila+="<td>" + json['numbrTelefono'] + "</td>";
                     nuevaFila+="<td>" + json['txtCorreo'] + "</td>";
					 nuevaFila+="<td><input type='button' value='Editar' class='btnEditar' onclick='litarEditar("+ (trs) +")'><input type='button' value='Eliminar' class='btnEliminar' onclick='eliminar("+ (trs) +")'></td>";
					 nuevaFila+="</tr>";
					 $("table").append(nuevaFila);
					}else{
					 $('#pos' + pos).children('td')[0].innerHTML = json['txtNombre'];
                     $('#pos' + pos).children('td')[1].innerHTML = json['numbrNumeroDoc'];
					 $('#pos' + pos).children('td')[2].innerHTML = json['slcTipo'];
                     $('#pos' + pos).children('td')[3].innerHTML = json['numbrTelefono'];
					 $('#pos' + pos).children('td')[4].innerHTML = json['txtCorreo'];
					}
			}
            }
		  });
		}
  }
  
  function litarEditar(id){
   var form = new FormData($('form')[1]);
   json = {
    'id': id,
    'accion': 2
   }
      $.ajax({
        url: '../../Controlador/Seguridad/profesionalController.php',
        type: 'POST',
        dataType: 'json',
        data: json,
        success: function (data){
            $("#txtNombre").val(data['datos']['nombre_profesional']);
            $("#numbrNumeroDoc").val(data['datos']['numero_documento_profesional']);
            $("#slcTipo").val(data['datos']['tipo_profesional']);
            $("#numbrTelefono").val(data['datos']['telefono_profesional']);
            $("#txtCorreo").val(data['datos']['correo_profesional']);
            $("#submit").attr("value","Editar");
            $("#form").attr("action", "javascript: guardar(3," + id + ")");
            
        }
      });
  }
  
  function limpiar(){
   $("form")[1].reset();
   $("#submit").attr("value","Agregar");
   $("#form").attr("action", "javascript: guardar(1,0);");
  }
  
function eliminar(id){
   if(confirm("Estas seguro?")){
    json = {
    'id': id,
    'accion': 4
   }
      $.ajax({
        url: '../../Controlador/Seguridad/profesionalController.php',
        type: 'POST',
        dataType: 'json',
        data: json,
        success: function (data){
            if(data['exito'] == 1){
                $('#pos' + id).remove();
                limpiar();
            }
        }
      });
   }
  }
