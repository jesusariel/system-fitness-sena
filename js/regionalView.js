function guardar(accion,pos){
    var json;
		json={
			"txtRegional": $('#txtRegional').val(),
			"slcDepartamento": $('#slcDepartamento').val(),
			"slcSubdirector": $('#slcSubdirector').val(),
			"slcEstado": $('#slcEstado').val(),
			'accion':accion,
			'id':pos
		}
      for(var indice in json){
		if((json[indice] == "")){
		  alert("Error: Se debe llenar todos los campos");
		  correcto = false;
		  break;
		}else{
		  correcto = true;
		}
	  }
		if(correcto){
			$.ajax({
			url:'../../../Controlador/ubicacion/regionalController.php',
			type:'POST',
			dataType:'json',
			data: json,
			success: function (data){
				alert(data["mensaje"]);
				if(data['exito'] == 1){
					limpiar();
					if(accion == 1){
					 // Obtenemos el total de columnas (tr) del id "tabla"
					 var trs=$("table tr").length;
					 var nuevaFila="<tr id='pos" + (trs) +"'>";
					 // añadimos las columnas
					 nuevaFila+="<td>" + json['txtRegional'] +"</td>";
					 nuevaFila+="<td>" + json['slcDepartamento'] + "</td>";
					 nuevaFila+="<td>" + json['slcSubdirector'] + "</td>";
					 nuevaFila+="<td><input type='button' value='Editar' class='btnEditar' onclick='litarEditar("+ (trs) +")'><input type='button' value='Eliminar' class='btnEliminar' onclick='eliminar("+ (trs) +")'></td>";
					 nuevaFila+="</tr>";
					 $("table").append(nuevaFila);
					}else{
					 $('#pos' + pos).children('td')[0].innerHTML = json['txtRegional'];
					 $('#pos' + pos).children('td')[1].innerHTML = json['slcDepartamento'];
					 $('#pos' + pos).children('td')[2].innerHTML = json['slcSubdirector'];
					}
			}
      }
		  });
		}
  }
  
  function litarEditar(id){
   var form = new FormData($('form')[1]);
   json = {
    'id': id,
    'accion': 2
   }
      $.ajax({
        url: '../../../Controlador/ubicacion/regionalController.php',
        type: 'POST',
        dataType: 'json',
        data: json,
        success: function (data){
            $("#txtRegional").val(data['datos']['regional_regional']);
            $("#slcDepartamento").val(data['datos']['departamento_regional']);
            $("#slcSubdirector").val(data['datos']['subdirector_regional']);
            $("#slcEstado").val(data['datos']['estado_regional']);
            $("#submit").attr("value","Editar");
            $("#form").attr("action", "javascript: guardar(3," + id + ")");
        }
      });
  }
  
  function limpiar(){
   $("form")[1].reset();
   $("#submit").attr("value","Agregar");
   $("#form").attr("action", "javascript: guardar(1,1);");
  }
  
function eliminar(id){
   if(confirm("Estas seguro?")){
    json = {
    'id': id,
    'accion': 4
   }
      $.ajax({
        url: '../../../Controlador/ubicacion/regionalController.php',
        type: 'POST',
        dataType: 'json',
        data: json,
        success: function (data){
            if(data['exito'] == 1){
                $('#pos' + id).remove();
                limpiar();
            }
        }
      });
   }
  }
